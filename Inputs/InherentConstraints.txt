* Check Feasibility:
	* Do we have enough spots for all games? (need to know # of games per team)
	* Can we schedule unique pairings, or do we have to double some matches?

* Sheets - no more than 2 games on a single sheet for a team (flexible)
* Times - no more thna 5 games in a time slot for a team (flexible)
