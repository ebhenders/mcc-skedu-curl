1. Create an initial schedule (for example, work forward, pairing teams until slots are filled)
	* should also assess feasiblilty of games and number of sheets available, whether re-matches need to happen or not
	
2. Add up penalty points:
	* Unnecessary rematches
	* Too many games on one sheet
	* Too many games in one time slot

3. Try permutations:
	* swap within-time pairing (within division)
	* swap between times for same pairing
	* swap between times for different pairings	
