<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddTeam
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.lbl1 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tbLeagueNum = New System.Windows.Forms.TextBox
        Me.tbTeamName = New System.Windows.Forms.TextBox
        Me.tbBye1 = New System.Windows.Forms.TextBox
        Me.tbBye2 = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lbl1
        '
        Me.lbl1.AutoSize = True
        Me.lbl1.Location = New System.Drawing.Point(16, 43)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(117, 13)
        Me.lbl1.TabIndex = 0
        Me.lbl1.Text = "Team League (number)"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Team Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Bye Request (optional)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 144)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(123, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Bye Request 2 (optional)"
        '
        'tbLeagueNum
        '
        Me.tbLeagueNum.Location = New System.Drawing.Point(166, 41)
        Me.tbLeagueNum.Name = "tbLeagueNum"
        Me.tbLeagueNum.Size = New System.Drawing.Size(41, 20)
        Me.tbLeagueNum.TabIndex = 1
        '
        'tbTeamName
        '
        Me.tbTeamName.Location = New System.Drawing.Point(166, 76)
        Me.tbTeamName.Name = "tbTeamName"
        Me.tbTeamName.Size = New System.Drawing.Size(257, 20)
        Me.tbTeamName.TabIndex = 2
        '
        'tbBye1
        '
        Me.tbBye1.Location = New System.Drawing.Point(166, 110)
        Me.tbBye1.Name = "tbBye1"
        Me.tbBye1.Size = New System.Drawing.Size(41, 20)
        Me.tbBye1.TabIndex = 3
        '
        'tbBye2
        '
        Me.tbBye2.Location = New System.Drawing.Point(166, 141)
        Me.tbBye2.Name = "tbBye2"
        Me.tbBye2.Size = New System.Drawing.Size(41, 20)
        Me.tbBye2.TabIndex = 4
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(64, 197)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 42)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Add"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(178, 197)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(83, 42)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'AddTeam
        '
        Me.AcceptButton = Me.Button1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(453, 262)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbBye2)
        Me.Controls.Add(Me.tbBye1)
        Me.Controls.Add(Me.tbTeamName)
        Me.Controls.Add(Me.tbLeagueNum)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbl1)
        Me.Name = "AddTeam"
        Me.Text = "AddTeam"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tbLeagueNum As System.Windows.Forms.TextBox
    Friend WithEvents tbTeamName As System.Windows.Forms.TextBox
    Friend WithEvents tbBye1 As System.Windows.Forms.TextBox
    Friend WithEvents tbBye2 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
