Public Class Mainform



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Schedule.Click
        Dim timepen As Integer
        Dim sheetpen As Integer
        Dim teamgamepen As Integer
        Dim timeoffpen As Integer
        Dim besttimepenalty As Integer
        Dim bestsheetpenalty As Integer
        Dim bestteamgamepenalty As Integer
        Dim bestteamtimeoffpenalty As Integer
        Dim bInfeasible As Boolean
        Dim kgoodcount As Integer
        Dim basefolder As String
        Dim OrigTeamPair(,) As Integer
        Dim kcounter As Integer
        Dim infeascounter As Integer
        'Dim RRResults(,) As Integer

        Randomize()

        iternum += 1
        If rbOverWrite.Checked = True Then iternum = 0
        SaveAs(iternum, basefolder)


        LowestPenaltyFound = 1000000
        infeascounter = 0

        ReDim BSWeekTimeSheetUsed(NWeeks, NTimes, NSheets), BSMatchWeekTimeSheetTeam1(NWeeks, NTimes, NSheets), BSMatchWeekTimeSheetTeam2(NWeeks, NTimes, NSheets)
        ReDim BSTeamTimePenalty(NTeams), BSTeamSheetPenalty(NTeams), BSTeamTimeOffPenalty(NTeams), BSNTeamWeek(NTeams, NWeeks)

        SetParameterValues()
        If CheckFeasibility() = False Then
            MsgBox("Not enough available times and/or sheets to accommodate all teams and games.")
            Exit Sub
        End If

        kgoodcount = 0
        kcounter = 0
        For jpair As Integer = 1 To NPairIters

            ScheduleTeamPairs2()

            CountTeamPenalties(teamgamepen)

            If teamgamepen = 0 Then
                ReDim OrigTeamPair(NTeams, NTeams)
                For jteam As Integer = 1 To NTeams
                    For jjteam As Integer = 1 To NTeams
                        OrigTeamPair(jteam, jjteam) = TeamPair(jteam, jjteam)
                    Next
                Next

                For jiter As Integer = 1 To NSearchIters
                    If kgoodcount > MaxSchedToWrite Then Exit For
                    'reset to original set of pairs
                    ReDim TeamPair(NTeams, NTeams)
                    For jteam As Integer = 1 To NTeams
                        For jjteam As Integer = 1 To NTeams
                            TeamPair(jteam, jjteam) = OrigTeamPair(jteam, jjteam)
                        Next
                    Next

                    ScheduleTimes(bInfeasible)
                    If bInfeasible Then ScheduleTimesOrdered(bInfeasible)

                    If bInfeasible = True Then infeascounter += 1
                    If bInfeasible = False Then
                        kcounter += 1
                        ReDim TeamSchedule(NTeams, NWeeks)
                        For jteam As Integer = 1 To NTeams
                            MakeTeamSchedule(jteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                        Next
                        CountPenalties(timepen, sheetpen, timeoffpen, teamgamepen)
                        RefineTimes()
                        FillOpenTimes()
                        TeamPairSwaps()
                        TeamSwaps()


                        CountPenalties(timepen, sheetpen, timeoffpen, teamgamepen)

                        If (timepen + sheetpen + timeoffpen <= LowestPenaltyFound) Or (timepen + sheetpen + timeoffpen = 0) Then

                            LowestPenaltyFound = Math.Min(LowestPenaltyFound, timepen + sheetpen + timeoffpen)
                            BSMatchWeekTimeSheetTeam1 = MatchWeekTimeSheetTeam1
                            BSMatchWeekTimeSheetTeam2 = MatchWeekTimeSheetTeam2
                            BSWeekTimeSheetUsed = WeekTimeSheetUsed
                            BSNTeamWeek = NTeamWeek
                            BSTeamTimePenalty = TeamTimePenalty
                            BSTeamSheetPenalty = TeamSheetPenalty
                            BSTeamTimeOffPenalty = TeamTimeOffPenalty

                            besttimepenalty = timepen
                            bestsheetpenalty = sheetpen
                            bestteamgamepenalty = teamgamepen
                            bestteamtimeoffpenalty = timeoffpen

                            If LowestPenaltyFound = 0 Or kcounter >= MinTriesBeforeWrite Then
                                kgoodcount += 1
                                WriteSchedule(iternum, besttimepenalty, bestsheetpenalty, bestteamtimeoffpenalty, bestteamgamepenalty, kgoodcount, basefolder)
                            End If
                        End If
                    End If
                Next jiter
            End If
        Next jpair

        If kgoodcount = 0 Then
            For jteam As Integer = 1 To NTeams
                MakeTeamSchedule(jteam, BSMatchWeekTimeSheetTeam1, BSMatchWeekTimeSheetTeam2, BSNTeamWeek)
            Next
            WriteSchedule(iternum, besttimepenalty, bestsheetpenalty, bestteamtimeoffpenalty, bestteamgamepenalty, kgoodcount, basefolder)

        End If

        MsgBox("Done with Penalty of: " & LowestPenaltyFound & " Failed Schedule Times: " & infeascounter)

    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim addtm As New Form
        Dim itm As String
        Team.bUsed = False
        addtm = New AddTeam
        addtm.ShowDialog()
        addtm = Nothing
        Dim sleague As String
        Dim sbye1 As String
        Dim sbye2 As String

        If bCancel = False Then
            sleague = CType(Team.LeagueNum, String)
            sbye1 = CType(Team.Bye1, String)
            sbye2 = CType(Team.Bye2, String)
            If Team.TeamName.Length > 35 Then Team.TeamName = Team.TeamName.Substring(0, 35)
            itm = sleague.PadRight(5, " ") & Team.TeamName.PadRight(40, " ") & sbye1.PadRight(5, " ") & sbye2.PadRight(5, " ")
            lbTeamLeague.Items.Add(itm)
            SetParameterValues()
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'lbTeamLeague.Items.RemoveAt(lbTeamLeague.SelectedItems.IndexOf)
        Dim i As Integer = lbTeamLeague.SelectedIndex
        For jitem As Integer = 0 To lbTeamLeague.SelectedItems.Count - 1
            lbTeamLeague.Items.Remove(lbTeamLeague.SelectedItem) '(jitem).Remove() 'Items.RemoveAt(jitem)
        Next
        lbTeamLeague.SelectedIndex = Math.Min(i, lbTeamLeague.Items.Count - 1)
        SetParameterValues()
    End Sub


    Public Sub SetParameterValues()
        Dim itm As String
        Dim DivName() As String
        Dim kdiv As Integer

        NTeams = lbTeamLeague.Items.Count

        ReDim TeamDiv(NTeams), TeamName(NTeams), TeamBye1(NTeams), TeamBye2(NTeams)
        NDivs = 0
        ReDim DivName(NDivs), TeamsPerDiv(NDivs)
        For jteam As Integer = 1 To lbTeamLeague.Items.Count
            itm = lbTeamLeague.Items(jteam - 1)
            kdiv = Array.IndexOf(DivName, Trim(itm.Substring(0, 5)))
            If kdiv < 0 Then
                NDivs += 1
                ReDim Preserve DivName(NDivs), TeamsPerDiv(NDivs)
                kdiv = NDivs
            End If
            DivName(kdiv) = kdiv
            TeamDiv(jteam) = kdiv
            TeamsPerDiv(kdiv) += 1
            TeamName(jteam) = Trim(itm.Substring(5, 40))
            TeamBye1(jteam) = CType(Trim(itm.Substring(45, 5)), Integer)
            TeamBye2(jteam) = CType(Trim(itm.Substring(50, 5)), Integer)

        Next

        tbNDivs.Text = NDivs
        If NDivs > 0 Then
            If TeamsPerDiv(1) > 0 Then tbNTeamsDiv1.Text = TeamsPerDiv(1)
        End If
        tbNTeamsDiv2.Visible = False
        tbNTeamsDiv2.Text = 0
        lblLeague2.Visible = False
        tbNTeamsDiv3.Visible = False
        tbNTeamsDiv3.Text = 0
        lblLeague3.Visible = False

        If NDivs > 1 Then
            tbNTeamsDiv2.Text = TeamsPerDiv(2)
            tbNTeamsDiv2.Visible = True
            lblLeague2.Visible = True
        End If
        If NDivs > 2 Then
            tbNTeamsDiv3.Text = TeamsPerDiv(3)
            tbNTeamsDiv3.Visible = True
            lblLeague3.Visible = True
        End If

        NSearchIters = CType(tbScheduleTests.Text, Integer)
        NPairIters = CType(tbPairTests.Text, Integer)
        MaxSchedToWrite = CType(tbMaxSchedToWrite.Text, Integer)

        NWeeks = CType(tbNWeeks.Text, Integer)

        MaxPerTime = CType(tbMaxTimePen.Text, Integer)
        MaxPerSheet = CType(tbMaxSheetPen.Text, Integer)
        MaxTimeOff = CType(tbMaxTimeBetweenPen.Text, Integer)

        ReDim TeamPair(NTeams, NTeams)

        ReDim NTeamWeek(NTeams, NWeeks)
        ReDim NTeamSheet(NTeams, NSheets)
        ReDim NTeamTime(NTeams, NTimes)
        ReDim NTeamGames(NTeams)
        ReDim GamesMatchedPerDiv(NDivs)

        ReDim NGamesPerTeamDiv(NDivs)
        For jdiv As Integer = 1 To NDivs
            NGamesPerTeamDiv(jdiv) = CType(tbNGames.Text, Integer)
        Next


        NTimes = lbTimeSlots.Items.Count
        NSheets = lbSheets.Items.Count
        tbNTimes.Text = NTimes
        tbNSheets.Text = NSheets

        ReDim TimePenalty(NTimes), TimeName(NTimes)
        ReDim SheetPenalty(NSheets), SheetName(NSheets)
        ReDim TeamSchedule(NTeams, NWeeks)
        For jtime As Integer = 1 To lbTimeSlots.Items.Count
            itm = lbTimeSlots.Items(jtime - 1)
            TimeName(jtime) = Trim(itm.Substring(0, 10))
            TimePenalty(jtime) = CType(Trim(itm.Substring(10, itm.Length - 10)), Integer)
        Next
        For jsheet As Integer = 1 To lbSheets.Items.Count
            itm = lbSheets.Items(jsheet - 1)
            SheetName(jsheet) = Trim(itm.Substring(0, 10))
            SheetPenalty(jsheet) = CType(Trim(itm.Substring(10, itm.Length - 10)), Integer)
        Next

        NSlots = NTimes * NSheets

        ReDim SlotPenalty(NSlots), SlotTime(NSlots), SlotSheet(NSlots)
        Dim kslot As Integer = 0
        For jtime As Integer = 1 To NTimes
            For jsheet As Integer = 1 To NSheets
                kslot += 1
                SlotPenalty(kslot) = TimePenalty(jtime) + SheetPenalty(jsheet)
                SlotTime(kslot) = jtime
                SlotSheet(kslot) = jsheet
            Next
        Next


        ''PopulateDivs()


    End Sub

    Public Function CheckFeasibility() As Boolean
        CheckFeasibility = True
        Dim NFields As Integer


        ReDim GamesNeededPerDiv(NDivs)
        NFields = NWeeks * NTimes * NSheets
        NGamesNeeded = 0
        MaxDivGamesNeeded = 0
        For jdiv As Integer = 1 To NDivs
            GamesNeededPerDiv(jdiv) = NGamesPerTeamDiv(jdiv) * TeamsPerDiv(jdiv)
            If (NGamesPerTeamDiv(jdiv) * TeamsPerDiv(jdiv)) Mod 2 > 0 Then
                GamesNeededPerDiv(jdiv) += 1
            End If
            GamesNeededPerDiv(jdiv) = GamesNeededPerDiv(jdiv) / 2
            MaxDivGamesNeeded = Math.Max(MaxDivGamesNeeded, GamesNeededPerDiv(jdiv))
            NGamesNeeded += GamesNeededPerDiv(jdiv)
        Next
        If NGamesNeeded > NFields Then CheckFeasibility = False
        NOpenSheets = NFields - NGamesNeeded
        MaxDivGamesNeeded = MaxDivGamesNeeded

    End Function
    'Private Sub PopulateDivs()
    '    TeamsPerDiv(1) = CType(tbNTeamsDiv1.Text, Integer)
    '    TeamsPerDiv(2) = CType(tbNTeamsDiv2.Text, Integer)
    '    For jteam As Integer = 1 To TeamsPerDiv(1)
    '        TeamDiv(jteam) = 1

    '    Next
    '    For jteam As Integer = TeamsPerDiv(1) + 1 To TeamsPerDiv(1) + TeamsPerDiv(2)
    '        TeamDiv(jteam) = 2
    '    Next
    '    For jdiv As Integer = 1 To NDivs
    '        GamesNeededPerDiv(jdiv) = NGamesPerTeamDiv(jdiv) * TeamsPerDiv(jdiv)
    '    Next
    'End Sub

    Sub WriteSchedule(ByVal iternum As Integer, ByVal timepen As Integer, ByVal sheetpen As Integer, ByVal timeoffpen As Integer, ByVal teamgamepen As Integer, ByVal solnum As Integer, ByVal outdir As String)

        Dim teampoints As Integer
        Dim totalpoints As Integer
        Dim minpoints As Integer = 10000000
        Dim maxpoints As Integer = 0
        Dim TotalPen As Integer = timepen + sheetpen + timeoffpen + teamgamepen
        Dim kteam As Integer
        Dim kkteam As Integer
        Dim unscheduledbyes As Integer
        'Dim nopensheets As Integer = 0

        Dim team1name As String
        Dim team2name As String

        AssignTeamSchedule(unscheduledbyes)
        SaveScenario()
        NOpenSheets = 0
        For jweek As Integer = 1 To NWeeks
            For jtime As Integer = 1 To NTimes
                For jsheet As Integer = 1 To NSheets
                    If BSWeekTimeSheetUsed(jweek, jtime, jsheet) = 0 Then NOpenSheets += 1
                Next
            Next
        Next

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, outdir & iternum & "Schedule_" & TotalPen & "_" & solnum & ".txt", OpenMode.Output)
        PrintLine(fnum, "TimePenalties," & timepen & ",Sheet_Penalties," & sheetpen & ",Time_Off_Penalty," & timeoffpen & ",Team_Game_Penalty," & teamgamepen & ",Total_Penalty," & TotalPen)
        PrintLine(fnum, "Open_Sheets," & nopensheets)
        PrintLine(fnum, "")
        PrintLine(fnum, "Week,Time,Sheet,Team1,Team2")
        For jweek As Integer = 1 To NWeeks
            For jtime As Integer = 1 To NTimes
                For jsheet As Integer = 1 To NSheets
                    If BSWeekTimeSheetUsed(jweek, jtime, jsheet) = 1 Then
                        kteam = BSMatchWeekTimeSheetTeam1(jweek, jtime, jsheet)
                        kkteam = BSMatchWeekTimeSheetTeam2(jweek, jtime, jsheet)
                        team1name = TeamName(TeamNumber(BSMatchWeekTimeSheetTeam1(jweek, jtime, jsheet)))
                        team2name = TeamName(TeamNumber(BSMatchWeekTimeSheetTeam2(jweek, jtime, jsheet)))
                        PrintLine(fnum, jweek & "," & TimeName(jtime) & "," & SheetName(jsheet) & "," & team1name & "," & team2name & "," & kteam & "," & kkteam) 'BSMatchWeekTimeSheetTeam1(jweek, jtime, jsheet) & "," & BSMatchWeekTimeSheetTeam2(jweek, jtime, jsheet))
                    Else
                        PrintLine(fnum, jweek & "," & TimeName(jtime) & "," & SheetName(jsheet) & ",Open,Open")
                    End If
                Next
            Next
            PrintLine(fnum, "")
        Next
        PrintLine(fnum, "Team,Opponent,Week,Time,Sheet")
        For jteam As Integer = 1 To NTeams
            For jweek As Integer = 1 To NWeeks
                For jtime As Integer = 1 To NTimes
                    For jsheet As Integer = 1 To NSheets
                        If BSWeekTimeSheetUsed(jweek, jtime, jsheet) = 1 And BSMatchWeekTimeSheetTeam1(jweek, jtime, jsheet) = jteam Then
                            kteam = BSMatchWeekTimeSheetTeam1(jweek, jtime, jsheet)
                            kkteam = BSMatchWeekTimeSheetTeam2(jweek, jtime, jsheet)
                            team1name = TeamName(TeamNumber(BSMatchWeekTimeSheetTeam1(jweek, jtime, jsheet)))
                            team2name = TeamName(TeamNumber(BSMatchWeekTimeSheetTeam2(jweek, jtime, jsheet)))
                            PrintLine(fnum, team1name & "," & team2name & "," & jweek & "," & TimeName(jtime) & "," & SheetName(jsheet) & "," & kteam & "," & kkteam)
                        ElseIf BSWeekTimeSheetUsed(jweek, jtime, jsheet) = 1 And BSMatchWeekTimeSheetTeam2(jweek, jtime, jsheet) = jteam Then
                            kteam = BSMatchWeekTimeSheetTeam2(jweek, jtime, jsheet)
                            kkteam = BSMatchWeekTimeSheetTeam1(jweek, jtime, jsheet)
                            team1name = TeamName(TeamNumber(BSMatchWeekTimeSheetTeam2(jweek, jtime, jsheet)))
                            team2name = TeamName(TeamNumber(BSMatchWeekTimeSheetTeam1(jweek, jtime, jsheet)))
                            PrintLine(fnum, team1name & "," & team2name & "," & jweek & "," & TimeName(jtime) & "," & SheetName(jsheet) & "," & kteam & "," & kkteam)
                        End If
                    Next
                Next
            Next
            PrintLine(fnum, "")
        Next
        PrintLine(fnum, "")
        PrintLine(fnum, "Team,Team_Number,Time_Penalties,Sheet_Penalties,Time_Off_Penalties,PenaltyPoints")
        For jteam As Integer = 1 To NTeams
            CountTeamPoints(jteam, TeamSchedule, teampoints)
            PrintLine(fnum, TeamName(TeamNumber(jteam)) & "," & jteam & "," & BSTeamTimePenalty(jteam) & "," & BSTeamSheetPenalty(jteam) & "," & BSTeamTimeOffPenalty(jteam) & "," & teampoints)
            totalpoints += teampoints
            minpoints = Math.Min(minpoints, teampoints)
            maxpoints = Math.Max(maxpoints, teampoints)
        Next
        PrintLine(fnum, "")
        PrintLine(fnum, "Total_Points," & totalpoints)
        PrintLine(fnum, "")
        PrintLine(fnum, "Team,Bye1,Bye2,Scheduled1,Scheduled2")
        For jteam As Integer = 1 To NTeams
            If TeamBye1(TeamNumber(jteam)) > 0 Then
                PrintLine(fnum, TeamName(TeamNumber(jteam)) & "," & TeamBye1(TeamNumber(jteam)) & "," & TeamBye2(TeamNumber(jteam)) & "," & TeamByeScheduled(jteam, 1) & "," & TeamByeScheduled(jteam, 2))
            End If
        Next
        FileClose(fnum)

        'stats file
        fnum = FreeFile()

        FileOpen(fnum, outdir & "ScheduleStats.txt", OpenMode.Append)
        PrintLine(fnum, "Schedule_" & TotalPen & "_" & solnum & "," & minpoints & "," & maxpoints & "," & totalpoints & "," & unscheduledbyes)
        FileClose(fnum)

    End Sub

    Private Sub SaveScenario()
        Dim basefolder As String
        Dim savefile As String
        basefolder = tbOutputFile.Text
        If System.IO.Directory.Exists(basefolder) = False Then
            Dim SaveFileDialog1 As New SaveFileDialog


            With SaveFileDialog1
                .Title = "Place to Write Schedules"
                .FileName = "ScenarioFile"
                .Filter = "Comma-delimited files(.skscn) |*.skscn"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With


            tbOutputFile.Text = DirName(SaveFileDialog1.FileName)
            basefolder = DirName(SaveFileDialog1.FileName)
            savefile = SaveFileDialog1.FileName

            SaveFileDialog1 = Nothing
        End If
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, basefolder & iternum & "ScenarioFile.skscn", OpenMode.Output)
        'PrintLine(fnum, "TimePenalties," & timepen & ",Sheet_Penalties," & sheetpen & ",Time_Off_Penalty," & timeoffpen & ",Team_Game_Penalty," & teamgamepen & ",Total_Penalty," & TotalPen)
        PrintLine(fnum, "Scenario File for: " & basefolder & iternum)
        PrintLine(fnum, "Run on : " & DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))
        PrintLine(fnum, "") 'blank line
        PrintLine(fnum, "Run Parameters")
        PrintLine(fnum, "NWeeks," & tbNWeeks.Text)
        PrintLine(fnum, "NGames," & tbNGames.Text)
        PrintLine(fnum, "") 'blank line
        PrintLine(fnum, "NTeams," & lbTeamLeague.Items.Count)
        PrintLine(fnum, "Division,Team,Bye1,Bye2")
        For jitem As Integer = 0 To lbTeamLeague.Items.Count - 1
            PrintLine(fnum, lbTeamLeague.Items(jitem))
        Next
        PrintLine(fnum, "") 'blank line
        PrintLine(fnum, "Penalties")
        PrintLine(fnum, "MaxTimePenalties," & tbMaxTimePen.Text)
        PrintLine(fnum, "MaxSheetPenalties," & tbMaxSheetPen.Text)
        PrintLine(fnum, "MaxTimeOffPenalties," & tbMaxTimeBetweenPen.Text)
        PrintLine(fnum, "") 'blank line
        PrintLine(fnum, "Times")
        PrintLine(fnum, "NTimes," & lbTimeSlots.Items.Count)
        PrintLine(fnum, "Time, Penalty")
        For jitem As Integer = 0 To lbTimeSlots.Items.Count - 1
            PrintLine(fnum, lbTimeSlots.Items(jitem))
        Next
        PrintLine(fnum, "") 'blank line
        PrintLine(fnum, "Sheets")
        PrintLine(fnum, "NSheets," & lbSheets.Items.Count)
        PrintLine(fnum, "Sheet, Penalty")
        For jitem As Integer = 0 To lbSheets.Items.Count - 1
            PrintLine(fnum, lbSheets.Items(jitem))
        Next
        PrintLine(fnum, "") 'blank line
        PrintLine(fnum, "Solver Parameters")
        PrintLine(fnum, "PairingTests," & tbPairTests.Text)
        PrintLine(fnum, "ScheduleTests," & tbScheduleTests.Text)
        PrintLine(fnum, "") 'blank line
        PrintLine(fnum, "Output Folder," & tbOutputFile.Text)
        PrintLine(fnum, "Overwrite," & rbOverWrite.Checked)
        PrintLine(fnum, "Add_To_Solutions," & rbAddSolutions.Checked)
        PrintLine(fnum, "") 'blank line
        PrintLine(fnum, "Max_N_Schedules_To_Write," & tbMaxSchedToWrite.Text)
        FileClose(fnum)
    End Sub

    Private Sub LoadScenario(ByVal skscn As String)
        Dim basefolder As String

        Dim dummy As String
        Dim arr() As String

        basefolder = tbOutputFile.Text

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, skscn, OpenMode.Input)
        'PrintLine(fnum, "TimePenalties," & timepen & ",Sheet_Penalties," & sheetpen & ",Time_Off_Penalty," & timeoffpen & ",Team_Game_Penalty," & teamgamepen & ",Total_Penalty," & TotalPen)
        LineInput(fnum) ' PrintLine(fnum, "Scenario File for: " & basefolder & iternum)
        LineInput(fnum) ' PrintLine(fnum, "Run on : " & DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))
        LineInput(fnum) ' PrintLine(fnum, "") 'blank line
        LineInput(fnum) 'PrintLine(fnum, "Run Parameters")
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "NWeeks," & tbNWeeks.Text)
        tbNWeeks.Text = arr(1)
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "NGames," & tbNGames.Text)
        tbNGames.Text = arr(1)
        LineInput(fnum) 'PrintLine(fnum, "") 'blank line
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "NTeams," & lbTeamLeague.Items.Count)
        NTeams = arr(1)
        LineInput(fnum) 'PrintLine(fnum, "Division,Team,Bye1,Bye2")
        For jitem As Integer = 1 To NTeams
            dummy = LineInput(fnum)
            lbTeamLeague.Items.Add(dummy)
            'PrintLine(fnum, lbTeamLeague.Items(jitem))
        Next
        LineInput(fnum) 'PrintLine(fnum, "") 'blank line
        LineInput(fnum) 'PrintLine(fnum, "Penalties")
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "MaxTimePenalties," & tbMaxTimePen.Text)
        tbMaxTimePen.Text = arr(1)
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "MaxSheetPenalties," & tbMaxSheetPen.Text)
        tbMaxSheetPen.Text = arr(1)
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "MaxTimeOffPenalties," & tbMaxTimeBetweenPen.Text)
        tbMaxTimeBetweenPen.Text = arr(1)
        LineInput(fnum) 'PrintLine(fnum, "") 'blank line
        LineInput(fnum) 'PrintLine(fnum, "Times")
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "NTimes," & lbTimeSlots.Items.Count)
        NTimes = arr(1)
        LineInput(fnum) 'PrintLine(fnum, "Time, Penalty")
        For jitem As Integer = 1 To NTimes
            dummy = LineInput(fnum)
            lbTimeSlots.Items.Add(dummy)
            'PrintLine(fnum, lbTimeSlots.Items(jitem))
        Next
        LineInput(fnum) ' PrintLine(fnum, "") 'blank line
        LineInput(fnum) 'PrintLine(fnum, "Sheets")
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "NSheets," & lbSheets.Items.Count)
        NSheets = arr(1)
        LineInput(fnum) 'PrintLine(fnum, "Sheet, Penalty")
        For jitem As Integer = 1 To NSheets 'lbTimeSlots.Items.Count - 1
            dummy = LineInput(fnum)
            lbSheets.Items.Add(dummy)
            'PrintLine(fnum, lbSheets.Items(jitem))
        Next
        LineInput(fnum) 'PrintLine(fnum, "") 'blank line
        LineInput(fnum) 'PrintLine(fnum, "Solver Parameters")
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "PairingTests," & tbPairTests.Text)
        tbPairTests.Text = arr(1)
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "ScheduleTests," & tbScheduleTests.Text)
        tbScheduleTests.Text = arr(1)
        LineInput(fnum) 'PrintLine(fnum, "") 'blank line
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "Output Folder," & tbOutputFile.Text)
        tbOutputFile.Text = arr(1)
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "Overwrite," & rbOverWrite.Checked)
        rbOverWrite.Checked = arr(1)
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "Add_To_Solutions," & rbAddSolutions.Checked)
        rbAddSolutions.Checked = arr(1)
        If EOF(fnum) Then
            FileClose(fnum)
            Exit Sub
        End If
        'owtherwise, the max schedules to write
        LineInput(fnum)
        arr = Split(LineInput(fnum), ",") 'PrintLine(fnum, "Add_To_Solutions," & rbAddSolutions.Checked)
        tbMaxSchedToWrite.Text = arr(1)
        FileClose(fnum)
    End Sub

    Private Sub SaveThisScenarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveThisScenarioToolStripMenuItem.Click
 
        SaveScenario()

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim addtm As New Form
        Dim itm As String
        Dim itmindex As Integer
        'Dim arr() As String
        'ReDim arr(4)
        If lbTeamLeague.SelectedItems.Count < 1 Then Exit Sub
        itm = lbTeamLeague.SelectedItem()
        itmindex = lbTeamLeague.SelectedIndex
        Team.LeagueNum = Trim(itm.Substring(0, 5))
        Team.TeamName = Trim(itm.Substring(5, 40))
        Team.Bye1 = Trim(itm.Substring(45, 5))
        Team.Bye2 = Trim(itm.Substring(50, 5))
        Team.bUsed = True

        addtm = New AddTeam
        addtm.ShowDialog()
        addtm = Nothing
        Dim sleague As String
        Dim sbye1 As String
        Dim sbye2 As String
        sleague = CType(Team.LeagueNum, String)
        sbye1 = CType(Team.Bye1, String)
        sbye2 = CType(Team.Bye2, String)

        If bCancel = False Then
            If Team.TeamName.Length > 35 Then Team.TeamName = Team.TeamName.Substring(0, 35)
            itm = sleague.PadRight(5, " ") & Team.TeamName.PadRight(40, " ") & sbye1.PadRight(5, " ") & sbye2.PadRight(5, " ")
            lbTeamLeague.Items(itmindex) = itm
            SetParameterValues()
        End If
    End Sub

    Public Sub MoveItem(ByVal lbtomove As Windows.Forms.ListBox, ByVal direction As Integer)

        'Checking selected item
        If (lbtomove.SelectedItem = "" Or lbtomove.SelectedIndex < 0) Then
            Return ' No selected item - nothing to do
        End If

        'Calculate new index using move direction
        Dim newIndex As Integer = lbtomove.SelectedIndex + direction

        'Checking bounds of the range
        If (newIndex < 0 Or newIndex >= lbtomove.Items.Count) Then
            Return ' Index out of range - nothing to do
        End If

        Dim selected As Object = lbtomove.SelectedItem

        'Removing removable element
        lbtomove.Items.Remove(selected)
        'Insert it in new position
        lbtomove.Items.Insert(newIndex, selected)
        'Restore selection
        lbtomove.SetSelected(newIndex, True)

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoveUp.Click
        MoveItem(lbTeamLeague, -1)
    End Sub

    Private Sub btnMoveDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoveDown.Click
        MoveItem(lbTeamLeague, 1)
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        MoveItem(lbTimeSlots, -1)
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        MoveItem(lbTimeSlots, 1)
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        MoveItem(lbSheets, -1)
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        MoveItem(lbSheets, 1)
    End Sub

    Private Sub LoadScenarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadScenarioToolStripMenuItem.Click
        Dim scenariofile As String
        Dim OpenFileDialog1 As New OpenFileDialog

        With OpenFileDialog1
            .Title = "Open a Scenario File"
            '.FileName = "Scenario"
            .Filter = "Comma-delimited files(.skscn) |*.skscn"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        scenariofile = OpenFileDialog1.FileName
        OpenFileDialog1 = Nothing

        lbTeamLeague.Items.Clear()
        lbTimeSlots.Items.Clear()
        lbSheets.Items.Clear()

        LoadScenario(scenariofile)
        SetParameterValues()


    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        For jitem As Integer = 0 To lbTimeSlots.SelectedItems.Count - 1
            lbTimeSlots.Items.Remove(lbTimeSlots.SelectedItem)
            SetParameterValues()
        Next
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        For jitem As Integer = 0 To lbSheets.SelectedItems.Count - 1
            lbSheets.Items.Remove(lbSheets.SelectedItem) 'RemoveAt(jitem)
            SetParameterValues()
        Next
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Dim addtm As New Form
        Dim itm As String
        TimeSheet.bUsed = False
        TimeSheet.TimeOrSheet = "Time"
        SheetOrTimeLabel = "Add Time"
        addtm = New AddSheetOrTime
        addtm.ShowDialog()
        addtm = Nothing
        Dim stime As String
        Dim ipen As String

        If bCancel = False Then
            stime = CType(TimeSheet.Name, String)
            ipen = CType(TimeSheet.Penalty, String)
            itm = stime.PadRight(10, " ") & ipen.PadRight(5)
            lbTimeSlots.Items.Add(itm)
            SetParameterValues()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim addtm As New Form
        Dim itm As String
        Dim itmindex As Integer
        'Dim arr() As String
        'ReDim arr(4)
        If lbTimeSlots.SelectedItems.Count < 1 Then Exit Sub
        itm = lbTimeSlots.SelectedItem()
        itmindex = lbTimeSlots.SelectedIndex
        TimeSheet.TimeOrSheet = "Time"
        SheetOrTimeLabel = "Edit Time"
        TimeSheet.Name = Trim(itm.Substring(0, 10))
        TimeSheet.Penalty = Trim(itm.Substring(10, itm.Length - 10))
        TimeSheet.bUsed = True

        addtm = New AddSheetOrTime
        addtm.ShowDialog()
        addtm = Nothing
        Dim stime As String
        Dim ipen As String

        If bCancel = False Then
            stime = CType(TimeSheet.Name, String)
            ipen = CType(TimeSheet.Penalty, String)
            itm = stime.PadRight(10, " ") & ipen.PadRight(5)
            'lbTimeSlots.Items.Add(itm)
            lbTimeSlots.Items(itmindex) = itm
            SetParameterValues()
        End If
    End Sub

    Private Sub Button5_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim addtm As New Form
        Dim itm As String
        TimeSheet.bUsed = False
        TimeSheet.TimeOrSheet = "Sheet"
        SheetOrTimeLabel = "Add Sheet"
        addtm = New AddSheetOrTime
        addtm.ShowDialog()
        addtm = Nothing
        Dim stime As String
        Dim ipen As String

        If bCancel = False Then
            stime = CType(TimeSheet.Name, String)
            ipen = CType(TimeSheet.Penalty, String)
            itm = stime.PadRight(10, " ") & ipen.PadRight(5)
            lbSheets.Items.Add(itm)
            SetParameterValues()
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Dim addtm As New Form
        Dim itm As String
        Dim itmindex As Integer
        If lbSheets.SelectedItems.Count < 1 Then Exit Sub
        itm = lbSheets.SelectedItem()
        itmindex = lbSheets.SelectedIndex
        TimeSheet.TimeOrSheet = "Sheet"
        SheetOrTimeLabel = "Edit Sheet"
        TimeSheet.Name = Trim(itm.Substring(0, 10))
        TimeSheet.Penalty = Trim(itm.Substring(10, itm.Length - 10))
        TimeSheet.bUsed = True

        addtm = New AddSheetOrTime
        addtm.ShowDialog()
        addtm = Nothing
        Dim stime As String
        Dim ipen As String

        If bCancel = False Then
            stime = CType(TimeSheet.Name, String)
            ipen = CType(TimeSheet.Penalty, String)
            itm = stime.PadRight(10, " ") & ipen.PadRight(5)
            lbSheets.Items(itmindex) = itm
            SetParameterValues()
        End If
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim basefolder As String
        SaveAs(0, basefolder)

    End Sub

    Private Sub SaveAs(ByVal inum As Integer, ByRef basefolder As String)

        Dim SaveFileDialog1 As New SaveFileDialog


        With SaveFileDialog1
            .Title = "Save This Scenario"
            .FileName = "ScenarioFile"
            .Filter = "Comma-delimited files(.skscn) |*.skscn"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With


        tbOutputFile.Text = DirName(SaveFileDialog1.FileName)
        basefolder = DirName(SaveFileDialog1.FileName)
        'If System.IO.File.Exists(outfile) Then LoadQueryBatchFile(outfile)

        SaveFileDialog1 = Nothing
        If inum = 0 Then
            Dim fnum As Integer = FreeFile()
            FileOpen(fnum, basefolder & "ScheduleStats.txt", OpenMode.Output)
            PrintLine(fnum, "RunNumber,MinPoints,MaxPoints,TotalPoints,UnscheduledByes")
            FileClose(fnum)
        End If
        SaveScenario()

    End Sub

    Private Sub SaveAsNewScenarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveAsNewScenarioToolStripMenuItem.Click
        Dim basefolder As String
        SaveAs(0, basefolder)
    End Sub

    Private Sub Mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbMaxSchedToWrite.TextChanged

    End Sub
End Class
