Module ModGlobalSubs


    Public bCancel As Boolean
    Public Team As TeamInfo
    Public TimeSheet As TimeOrSheet

    Public NTeams As Integer
    Public NDivs As Integer
    Public NGames As Integer

    Public NWeeks As Integer
    Public NTimes As Integer
    Public NSheets As Integer
    Public NSlots As Integer

    Public NGamesPerTeamDiv() As Integer 'by division; can differ the number of teams

    Public NSearchIters As Integer
    Public NPairIters As Integer

    Public MaxPerSheet As Integer
    Public MaxPerTime As Integer
    Public MaxTimeOff As Integer 'maximum interval between scheduled games

    Public TeamDiv() As Integer 'by NTeams, 
    Public TeamsPerDiv() As Integer

    Public TeamName() As String
    Public TeamNumber() As Integer
    Public TeamBye1() As Integer
    Public TeamBye2() As Integer
    Public TeamByeScheduled(,) As Integer
    Public SheetName() As String
    Public TimeName() As String

    Public TeamPair(,) As Integer 'number of times a team1,team2 plays each other - same value both ways
    Public NTeamWeek(,) As Integer 'by team, week - # of times a team plays in a week. Max 1 - hard constraint
    Public NTeamSheet(,) As Integer 'by team, sheet - # of times a team plays on a sheet. Max user-specified
    Public NTeamTime(,) As Integer 'by team, NTimes - # of tiems a team plays in a timeslot. Max user-specified
    Public NTeamGames() As Integer 'by team, number of games scheduled

    Public TeamSchedule(,) As Integer 'by team, NWeeks - returns timeslot

    '-----------------------------------------------------------
    Public TeamTimePenalty() As Integer
    Public TeamSheetPenalty() As Integer
    Public TeamTimeOffPenalty() As Integer
    Public BSTeamTimePenalty() As Integer
    Public BSTeamSheetPenalty() As Integer
    Public BSTeamTimeOffPenalty() As Integer
    Public BSNTeamWeek(,) As Integer 'best solution
    Public SheetPenalty() As Integer 'penalty points for playing on undesirable sheets
    Public TimePenalty() As Integer 'penalty points for playing at undesirable time
    Public SlotPenalty() As Integer 'ntimes*nsheets - cumulative penalty of time and sheet
    Public SlotTime() As Integer 'time of the slot
    Public SlotSheet() As Integer 'sheet of the slot

    Public GamesMatchedPerDiv() As Integer
    Public GamesNeededPerDiv() As Integer

    Public WeekTimeSheetUsed(,,) As Byte 'by week,time,sheet

    Public MatchWeekTimeSheetTeam1(,,) As Integer
    Public MatchWeekTimeSheetTeam2(,,) As Integer

    Public LowestPenaltyFound As Integer

    Public BSMatchWeekTimeSheetTeam1(,,) As Integer
    Public BSMatchWeekTimeSheetTeam2(,,) As Integer
    Public BSWeekTimeSheetUsed(,,) As Byte
    Public iternum As Integer = 0
    Public MinTriesBeforeWrite = 50
    Public MaxSchedToWrite As Integer
    Public LastTeamLeagueNum As Integer = 1

    Public NOpenSheets As Integer
    Public OrderedGameTeam1(,) As Integer 'by division, max number of games per div
    Public OrderedGameTeam2(,) As Integer
    Public MaxDivGamesNeeded As Integer 'maximum number of games to schedule in any division
    Public NGamesNeeded As Integer

    Public SheetOrTimeLabel As String

    Public Structure TeamInfo
        Dim LeagueNum As Integer
        Dim TeamName As String
        Dim Bye1 As Integer
        Dim Bye2 As Integer
        Dim bUsed As Boolean
    End Structure

    Public Structure TimeOrSheet
        Dim Name As String
        Dim Penalty As Integer
        Dim TimeOrSheet As String
        Dim bUsed As Integer
    End Structure

    Public Function DirName(ByVal FullName As String) As String
        'returns name of the directory with the "\"
        Dim i As Integer
        Dim dummy As String
        dummy = FullName
        i = 0
        DirName = ""
        'check for blank pass
        If Trim(FullName).Length = 0 Then Exit Function

        Do
            i = i + 1
            If dummy.Substring(Len(dummy) - i, 1) = "\" Then Exit Do
        Loop
        If i >= Len(dummy) Then
            DirName = ""
        Else
            DirName = dummy.Substring(0, (Len(dummy) - i))
        End If
        DirName = DirName & "\"
    End Function
End Module
