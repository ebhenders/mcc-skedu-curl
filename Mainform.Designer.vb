<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mainform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Mainform))
        Me.tbNGames = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Schedule = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Button4 = New System.Windows.Forms.Button
        Me.btnMoveUp = New System.Windows.Forms.Button
        Me.btnMoveDown = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Button11 = New System.Windows.Forms.Button
        Me.Button12 = New System.Windows.Forms.Button
        Me.Button13 = New System.Windows.Forms.Button
        Me.Button14 = New System.Windows.Forms.Button
        Me.rbOverWrite = New System.Windows.Forms.RadioButton
        Me.rbAddSolutions = New System.Windows.Forms.RadioButton
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.SolverParametersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LoadScenarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SaveThisScenarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SaveAsNewScenarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TabControl2 = New System.Windows.Forms.TabControl
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.lbTeamLeague = New System.Windows.Forms.ListBox
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.lbTimeSlots = New System.Windows.Forms.ListBox
        Me.lbSheets = New System.Windows.Forms.ListBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tbMaxTimeBetweenPen = New System.Windows.Forms.TextBox
        Me.tbMaxSheetPen = New System.Windows.Forms.TextBox
        Me.tbMaxTimePen = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.tbScheduleTests = New System.Windows.Forms.TextBox
        Me.tbPairTests = New System.Windows.Forms.TextBox
        Me.tbNTeamsDiv2 = New System.Windows.Forms.TextBox
        Me.lblLeague2 = New System.Windows.Forms.Label
        Me.tbNTeamsDiv1 = New System.Windows.Forms.TextBox
        Me.lblLeague1 = New System.Windows.Forms.Label
        Me.tbNDivs = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbNWeeks = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tbNTimes = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tbNSheets = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.tbOutputFile = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.tbNTeamsDiv3 = New System.Windows.Forms.TextBox
        Me.lblLeague3 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.tbMaxSchedToWrite = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.MenuStrip1.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbNGames
        '
        Me.tbNGames.Location = New System.Drawing.Point(116, 107)
        Me.tbNGames.Name = "tbNGames"
        Me.tbNGames.Size = New System.Drawing.Size(20, 20)
        Me.tbNGames.TabIndex = 0
        Me.tbNGames.Text = "7"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 110)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Games Per Team"
        '
        'Schedule
        '
        Me.Schedule.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Schedule.Location = New System.Drawing.Point(18, 444)
        Me.Schedule.Name = "Schedule"
        Me.Schedule.Size = New System.Drawing.Size(79, 55)
        Me.Schedule.TabIndex = 2
        Me.Schedule.Text = "Schedule"
        Me.ToolTip1.SetToolTip(Me.Schedule, "Click to generate schedule")
        Me.Schedule.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(86, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(391, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "League    Team Name                                   Bye 1   Bye 2"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(31, 43)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(28, 29)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "+"
        Me.ToolTip1.SetToolTip(Me.Button2, "Add a Team")
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Red
        Me.Button3.Location = New System.Drawing.Point(31, 78)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(28, 29)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "x"
        Me.ToolTip1.SetToolTip(Me.Button3, "Delete Selected")
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Red
        Me.Button4.Image = CType(resources.GetObject("Button4.Image"), System.Drawing.Image)
        Me.Button4.Location = New System.Drawing.Point(31, 113)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(28, 29)
        Me.Button4.TabIndex = 15
        Me.ToolTip1.SetToolTip(Me.Button4, "Edit Selected")
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btnMoveUp
        '
        Me.btnMoveUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMoveUp.Location = New System.Drawing.Point(28, 148)
        Me.btnMoveUp.Name = "btnMoveUp"
        Me.btnMoveUp.Size = New System.Drawing.Size(16, 28)
        Me.btnMoveUp.TabIndex = 16
        Me.btnMoveUp.Text = "^"
        Me.ToolTip1.SetToolTip(Me.btnMoveUp, "Move Up in list")
        Me.btnMoveUp.UseVisualStyleBackColor = True
        '
        'btnMoveDown
        '
        Me.btnMoveDown.Location = New System.Drawing.Point(47, 148)
        Me.btnMoveDown.Name = "btnMoveDown"
        Me.btnMoveDown.Size = New System.Drawing.Size(16, 28)
        Me.btnMoveDown.TabIndex = 17
        Me.btnMoveDown.Text = "v"
        Me.ToolTip1.SetToolTip(Me.btnMoveDown, "Move down in list")
        Me.btnMoveDown.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(331, 178)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(28, 29)
        Me.Button5.TabIndex = 22
        Me.Button5.Text = "+"
        Me.ToolTip1.SetToolTip(Me.Button5, "Add a Sheet")
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Red
        Me.Button8.Image = CType(resources.GetObject("Button8.Image"), System.Drawing.Image)
        Me.Button8.Location = New System.Drawing.Point(331, 247)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(28, 29)
        Me.Button8.TabIndex = 24
        Me.ToolTip1.SetToolTip(Me.Button8, "Edit Selected")
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.Red
        Me.Button9.Location = New System.Drawing.Point(331, 212)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(28, 29)
        Me.Button9.TabIndex = 23
        Me.Button9.Text = "x"
        Me.ToolTip1.SetToolTip(Me.Button9, "Delete Selected")
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Red
        Me.Button6.Image = CType(resources.GetObject("Button6.Image"), System.Drawing.Image)
        Me.Button6.Location = New System.Drawing.Point(39, 247)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(28, 29)
        Me.Button6.TabIndex = 28
        Me.ToolTip1.SetToolTip(Me.Button6, "Edit Selected")
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Red
        Me.Button7.Location = New System.Drawing.Point(39, 212)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(28, 29)
        Me.Button7.TabIndex = 27
        Me.Button7.Text = "x"
        Me.ToolTip1.SetToolTip(Me.Button7, "Delete Selected")
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(39, 178)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(28, 29)
        Me.Button10.TabIndex = 26
        Me.Button10.Text = "+"
        Me.ToolTip1.SetToolTip(Me.Button10, "Add a Time")
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(19, 61)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Scheduling Tests:"
        Me.ToolTip1.SetToolTip(Me.Label4, "For a set of matches, the number of unique schedules to examine")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Pairing Tests:"
        Me.ToolTip1.SetToolTip(Me.Label3, "Number of Different Sets of Matches")
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(54, 282)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(16, 28)
        Me.Button11.TabIndex = 32
        Me.Button11.Text = "v"
        Me.ToolTip1.SetToolTip(Me.Button11, "Move Down in list")
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.Location = New System.Drawing.Point(35, 282)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(16, 28)
        Me.Button12.TabIndex = 31
        Me.Button12.Text = "^"
        Me.ToolTip1.SetToolTip(Me.Button12, "Move Up in list")
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(346, 282)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(16, 28)
        Me.Button13.TabIndex = 34
        Me.Button13.Text = "v"
        Me.ToolTip1.SetToolTip(Me.Button13, "Move down in list")
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.Location = New System.Drawing.Point(327, 282)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(16, 28)
        Me.Button14.TabIndex = 33
        Me.Button14.Text = "^"
        Me.ToolTip1.SetToolTip(Me.Button14, "Move Up in list")
        Me.Button14.UseVisualStyleBackColor = True
        '
        'rbOverWrite
        '
        Me.rbOverWrite.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.rbOverWrite.AutoSize = True
        Me.rbOverWrite.Checked = True
        Me.rbOverWrite.Location = New System.Drawing.Point(136, 485)
        Me.rbOverWrite.Name = "rbOverWrite"
        Me.rbOverWrite.Size = New System.Drawing.Size(116, 17)
        Me.rbOverWrite.TabIndex = 17
        Me.rbOverWrite.TabStop = True
        Me.rbOverWrite.Text = "Overwrite Solutions"
        Me.ToolTip1.SetToolTip(Me.rbOverWrite, "Existing schedules in folder will be overwritten with every Schedule attempt")
        Me.rbOverWrite.UseVisualStyleBackColor = True
        '
        'rbAddSolutions
        '
        Me.rbAddSolutions.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.rbAddSolutions.AutoSize = True
        Me.rbAddSolutions.Location = New System.Drawing.Point(136, 503)
        Me.rbAddSolutions.Name = "rbAddSolutions"
        Me.rbAddSolutions.Size = New System.Drawing.Size(106, 17)
        Me.rbAddSolutions.TabIndex = 18
        Me.rbAddSolutions.Text = "Add To Solutions"
        Me.ToolTip1.SetToolTip(Me.rbAddSolutions, "Every Schedule attempt will add to the current set of schedules in the folder")
        Me.rbAddSolutions.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SolverParametersToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(894, 24)
        Me.MenuStrip1.TabIndex = 8
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SolverParametersToolStripMenuItem
        '
        Me.SolverParametersToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoadScenarioToolStripMenuItem, Me.SaveThisScenarioToolStripMenuItem, Me.SaveAsNewScenarioToolStripMenuItem})
        Me.SolverParametersToolStripMenuItem.Name = "SolverParametersToolStripMenuItem"
        Me.SolverParametersToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.SolverParametersToolStripMenuItem.Text = "Scenario"
        Me.SolverParametersToolStripMenuItem.ToolTipText = "Load or Save a scenario file (.skscn)"
        '
        'LoadScenarioToolStripMenuItem
        '
        Me.LoadScenarioToolStripMenuItem.Name = "LoadScenarioToolStripMenuItem"
        Me.LoadScenarioToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.LoadScenarioToolStripMenuItem.Text = "Load Scenario"
        '
        'SaveThisScenarioToolStripMenuItem
        '
        Me.SaveThisScenarioToolStripMenuItem.Name = "SaveThisScenarioToolStripMenuItem"
        Me.SaveThisScenarioToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.SaveThisScenarioToolStripMenuItem.Text = "Save This Scenario"
        '
        'SaveAsNewScenarioToolStripMenuItem
        '
        Me.SaveAsNewScenarioToolStripMenuItem.Name = "SaveAsNewScenarioToolStripMenuItem"
        Me.SaveAsNewScenarioToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.SaveAsNewScenarioToolStripMenuItem.Text = "Save As New Scenario"
        '
        'TabControl2
        '
        Me.TabControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Controls.Add(Me.TabPage1)
        Me.TabControl2.Location = New System.Drawing.Point(217, 27)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(609, 416)
        Me.TabControl2.TabIndex = 10
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.btnMoveDown)
        Me.TabPage3.Controls.Add(Me.btnMoveUp)
        Me.TabPage3.Controls.Add(Me.Button4)
        Me.TabPage3.Controls.Add(Me.lbTeamLeague)
        Me.TabPage3.Controls.Add(Me.Label2)
        Me.TabPage3.Controls.Add(Me.Button2)
        Me.TabPage3.Controls.Add(Me.Button3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(601, 390)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "Teams"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lbTeamLeague
        '
        Me.lbTeamLeague.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lbTeamLeague.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTeamLeague.FormattingEnabled = True
        Me.lbTeamLeague.ItemHeight = 15
        Me.lbTeamLeague.Location = New System.Drawing.Point(80, 34)
        Me.lbTeamLeague.Name = "lbTeamLeague"
        Me.lbTeamLeague.Size = New System.Drawing.Size(472, 349)
        Me.lbTeamLeague.TabIndex = 8
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Button13)
        Me.TabPage4.Controls.Add(Me.Button14)
        Me.TabPage4.Controls.Add(Me.Button11)
        Me.TabPage4.Controls.Add(Me.Button12)
        Me.TabPage4.Controls.Add(Me.Label16)
        Me.TabPage4.Controls.Add(Me.Label15)
        Me.TabPage4.Controls.Add(Me.Button6)
        Me.TabPage4.Controls.Add(Me.Button7)
        Me.TabPage4.Controls.Add(Me.Button10)
        Me.TabPage4.Controls.Add(Me.lbTimeSlots)
        Me.TabPage4.Controls.Add(Me.Button8)
        Me.TabPage4.Controls.Add(Me.Button9)
        Me.TabPage4.Controls.Add(Me.Button5)
        Me.TabPage4.Controls.Add(Me.lbSheets)
        Me.TabPage4.Controls.Add(Me.GroupBox1)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(601, 390)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "Times, Sheets, Penalties"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(371, 116)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(107, 15)
        Me.Label16.TabIndex = 30
        Me.Label16.Text = "Sheet    Penalty"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(79, 116)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(110, 15)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "Time      Penalty"
        '
        'lbTimeSlots
        '
        Me.lbTimeSlots.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTimeSlots.FormattingEnabled = True
        Me.lbTimeSlots.ItemHeight = 15
        Me.lbTimeSlots.Location = New System.Drawing.Point(82, 143)
        Me.lbTimeSlots.Name = "lbTimeSlots"
        Me.lbTimeSlots.Size = New System.Drawing.Size(182, 229)
        Me.lbTimeSlots.TabIndex = 25
        '
        'lbSheets
        '
        Me.lbSheets.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbSheets.FormattingEnabled = True
        Me.lbSheets.ItemHeight = 15
        Me.lbSheets.Location = New System.Drawing.Point(374, 143)
        Me.lbSheets.Name = "lbSheets"
        Me.lbSheets.Size = New System.Drawing.Size(182, 229)
        Me.lbSheets.TabIndex = 21
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tbMaxTimeBetweenPen)
        Me.GroupBox1.Controls.Add(Me.tbMaxSheetPen)
        Me.GroupBox1.Controls.Add(Me.tbMaxTimePen)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 11)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(204, 92)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        '
        'tbMaxTimeBetweenPen
        '
        Me.tbMaxTimeBetweenPen.Location = New System.Drawing.Point(170, 57)
        Me.tbMaxTimeBetweenPen.Name = "tbMaxTimeBetweenPen"
        Me.tbMaxTimeBetweenPen.Size = New System.Drawing.Size(20, 20)
        Me.tbMaxTimeBetweenPen.TabIndex = 19
        Me.tbMaxTimeBetweenPen.Text = "3"
        '
        'tbMaxSheetPen
        '
        Me.tbMaxSheetPen.Location = New System.Drawing.Point(170, 34)
        Me.tbMaxSheetPen.Name = "tbMaxSheetPen"
        Me.tbMaxSheetPen.Size = New System.Drawing.Size(20, 20)
        Me.tbMaxSheetPen.TabIndex = 18
        Me.tbMaxSheetPen.Text = "3"
        '
        'tbMaxTimePen
        '
        Me.tbMaxTimePen.Location = New System.Drawing.Point(170, 12)
        Me.tbMaxTimePen.Name = "tbMaxTimePen"
        Me.tbMaxTimePen.Size = New System.Drawing.Size(20, 20)
        Me.tbMaxTimePen.TabIndex = 17
        Me.tbMaxTimePen.Text = "4"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(11, 15)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(102, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Max Time Penalties:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(11, 37)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(107, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Max Sheet Penalties:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(11, 60)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(156, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Max Weeks Between Matches:"
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.tbMaxSchedToWrite)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.tbScheduleTests)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.tbPairTests)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(601, 390)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "Solver Parameters"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'tbScheduleTests
        '
        Me.tbScheduleTests.Location = New System.Drawing.Point(112, 58)
        Me.tbScheduleTests.Name = "tbScheduleTests"
        Me.tbScheduleTests.Size = New System.Drawing.Size(37, 20)
        Me.tbScheduleTests.TabIndex = 3
        Me.tbScheduleTests.Text = "200"
        '
        'tbPairTests
        '
        Me.tbPairTests.Location = New System.Drawing.Point(112, 32)
        Me.tbPairTests.Name = "tbPairTests"
        Me.tbPairTests.Size = New System.Drawing.Size(37, 20)
        Me.tbPairTests.TabIndex = 1
        Me.tbPairTests.Text = "10"
        '
        'tbNTeamsDiv2
        '
        Me.tbNTeamsDiv2.Enabled = False
        Me.tbNTeamsDiv2.Location = New System.Drawing.Point(108, 62)
        Me.tbNTeamsDiv2.Name = "tbNTeamsDiv2"
        Me.tbNTeamsDiv2.Size = New System.Drawing.Size(35, 20)
        Me.tbNTeamsDiv2.TabIndex = 14
        Me.tbNTeamsDiv2.Text = "0"
        '
        'lblLeague2
        '
        Me.lblLeague2.AutoSize = True
        Me.lblLeague2.Location = New System.Drawing.Point(5, 67)
        Me.lblLeague2.Name = "lblLeague2"
        Me.lblLeague2.Size = New System.Drawing.Size(93, 13)
        Me.lblLeague2.TabIndex = 13
        Me.lblLeague2.Text = "League 2 Teams: "
        '
        'tbNTeamsDiv1
        '
        Me.tbNTeamsDiv1.Enabled = False
        Me.tbNTeamsDiv1.Location = New System.Drawing.Point(108, 37)
        Me.tbNTeamsDiv1.Name = "tbNTeamsDiv1"
        Me.tbNTeamsDiv1.Size = New System.Drawing.Size(35, 20)
        Me.tbNTeamsDiv1.TabIndex = 12
        Me.tbNTeamsDiv1.Text = "0"
        '
        'lblLeague1
        '
        Me.lblLeague1.AutoSize = True
        Me.lblLeague1.Location = New System.Drawing.Point(5, 40)
        Me.lblLeague1.Name = "lblLeague1"
        Me.lblLeague1.Size = New System.Drawing.Size(87, 13)
        Me.lblLeague1.TabIndex = 11
        Me.lblLeague1.Text = "League1 Teams:"
        '
        'tbNDivs
        '
        Me.tbNDivs.Enabled = False
        Me.tbNDivs.Location = New System.Drawing.Point(108, 11)
        Me.tbNDivs.Name = "tbNDivs"
        Me.tbNDivs.Size = New System.Drawing.Size(35, 20)
        Me.tbNDivs.TabIndex = 10
        Me.tbNDivs.Text = "0"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(5, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(54, 13)
        Me.Label11.TabIndex = 9
        Me.Label11.Text = "Leagues: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 84)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Weeks"
        '
        'tbNWeeks
        '
        Me.tbNWeeks.Location = New System.Drawing.Point(116, 81)
        Me.tbNWeeks.Name = "tbNWeeks"
        Me.tbNWeeks.Size = New System.Drawing.Size(20, 20)
        Me.tbNWeeks.TabIndex = 11
        Me.tbNWeeks.Text = "12"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(1, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Times Per Week"
        '
        'tbNTimes
        '
        Me.tbNTimes.Enabled = False
        Me.tbNTimes.Location = New System.Drawing.Point(105, 13)
        Me.tbNTimes.Name = "tbNTimes"
        Me.tbNTimes.Size = New System.Drawing.Size(20, 20)
        Me.tbNTimes.TabIndex = 13
        Me.tbNTimes.Text = "0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(1, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Sheets Per Time"
        '
        'tbNSheets
        '
        Me.tbNSheets.Enabled = False
        Me.tbNSheets.Location = New System.Drawing.Point(105, 39)
        Me.tbNSheets.Name = "tbNSheets"
        Me.tbNSheets.Size = New System.Drawing.Size(20, 20)
        Me.tbNSheets.TabIndex = 15
        Me.tbNSheets.Text = "0"
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(136, 457)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(98, 13)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Results Written To:"
        '
        'tbOutputFile
        '
        Me.tbOutputFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tbOutputFile.Location = New System.Drawing.Point(253, 455)
        Me.tbOutputFile.Name = "tbOutputFile"
        Me.tbOutputFile.Size = New System.Drawing.Size(437, 20)
        Me.tbOutputFile.TabIndex = 20
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(707, 453)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(70, 22)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "Change"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tbNTeamsDiv3
        '
        Me.tbNTeamsDiv3.Enabled = False
        Me.tbNTeamsDiv3.Location = New System.Drawing.Point(108, 88)
        Me.tbNTeamsDiv3.Name = "tbNTeamsDiv3"
        Me.tbNTeamsDiv3.Size = New System.Drawing.Size(35, 20)
        Me.tbNTeamsDiv3.TabIndex = 23
        Me.tbNTeamsDiv3.Text = "0"
        '
        'lblLeague3
        '
        Me.lblLeague3.AutoSize = True
        Me.lblLeague3.Location = New System.Drawing.Point(5, 93)
        Me.lblLeague3.Name = "lblLeague3"
        Me.lblLeague3.Size = New System.Drawing.Size(93, 13)
        Me.lblLeague3.TabIndex = 22
        Me.lblLeague3.Text = "League 3 Teams: "
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(19, 93)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(127, 13)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Max Schedules To Write:"
        Me.ToolTip1.SetToolTip(Me.Label12, "For a set of matches, the number of unique schedules to examine")
        '
        'tbMaxSchedToWrite
        '
        Me.tbMaxSchedToWrite.Location = New System.Drawing.Point(152, 86)
        Me.tbMaxSchedToWrite.Name = "tbMaxSchedToWrite"
        Me.tbMaxSchedToWrite.Size = New System.Drawing.Size(37, 20)
        Me.tbMaxSchedToWrite.TabIndex = 5
        Me.tbMaxSchedToWrite.Text = "50"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.tbNTeamsDiv3)
        Me.GroupBox2.Controls.Add(Me.lblLeague3)
        Me.GroupBox2.Controls.Add(Me.tbNTeamsDiv2)
        Me.GroupBox2.Controls.Add(Me.lblLeague2)
        Me.GroupBox2.Controls.Add(Me.tbNTeamsDiv1)
        Me.GroupBox2.Controls.Add(Me.lblLeague1)
        Me.GroupBox2.Controls.Add(Me.tbNDivs)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 230)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(152, 122)
        Me.GroupBox2.TabIndex = 24
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.tbNSheets)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.tbNTimes)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 148)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(144, 76)
        Me.GroupBox3.TabIndex = 25
        Me.GroupBox3.TabStop = False
        '
        'Mainform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 532)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbOutputFile)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.rbAddSolutions)
        Me.Controls.Add(Me.rbOverWrite)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tbNWeeks)
        Me.Controls.Add(Me.TabControl2)
        Me.Controls.Add(Me.Schedule)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbNGames)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Mainform"
        Me.Text = "MCC Skedu-Curl"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbNGames As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Schedule As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents SolverParametersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents lbTeamLeague As System.Windows.Forms.ListBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents LoadScenarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveThisScenarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tbScheduleTests As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbPairTests As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbNWeeks As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbNTimes As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbNSheets As System.Windows.Forms.TextBox
    Friend WithEvents tbMaxTimeBetweenPen As System.Windows.Forms.TextBox
    Friend WithEvents tbMaxSheetPen As System.Windows.Forms.TextBox
    Friend WithEvents tbMaxTimePen As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbNTeamsDiv2 As System.Windows.Forms.TextBox
    Friend WithEvents lblLeague2 As System.Windows.Forms.Label
    Friend WithEvents tbNTeamsDiv1 As System.Windows.Forms.TextBox
    Friend WithEvents lblLeague1 As System.Windows.Forms.Label
    Friend WithEvents tbNDivs As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents rbOverWrite As System.Windows.Forms.RadioButton
    Friend WithEvents rbAddSolutions As System.Windows.Forms.RadioButton
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents tbOutputFile As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents btnMoveDown As System.Windows.Forms.Button
    Friend WithEvents btnMoveUp As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents lbTimeSlots As System.Windows.Forms.ListBox
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents lbSheets As System.Windows.Forms.ListBox
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents tbNTeamsDiv3 As System.Windows.Forms.TextBox
    Friend WithEvents lblLeague3 As System.Windows.Forms.Label
    Friend WithEvents SaveAsNewScenarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tbMaxSchedToWrite As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox

End Class
