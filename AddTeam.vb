Public Class AddTeam

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Team.LeagueNum = CType(Trim(tbLeagueNum.Text), Integer)
        LastTeamLeagueNum = Team.LeagueNum
        Team.TeamName = tbTeamName.Text
        Team.Bye1 = 0
        Team.Bye2 = 0
        If tbBye1.Text <> "" Then
            Team.Bye1 = CType(Trim(tbBye1.Text), Integer)
        End If
        If tbBye2.Text <> "" Then
            Team.Bye2 = CType(Trim(tbBye2.Text), Integer)
        End If

        bCancel = False
        Me.Close()


    End Sub

    Private Sub AddTeam_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        bCancel = True
        If Team.bUsed = True Then
            tbLeagueNum.Text = Team.LeagueNum
            tbTeamName.Text = Team.TeamName
            tbBye1.Text = Team.Bye1
            tbBye2.Text = Team.Bye2
        Else
            tbLeagueNum.Text = LastTeamLeagueNum
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bCancel = True
        Me.Close()
    End Sub
End Class