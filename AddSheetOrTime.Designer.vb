<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddSheetOrTime
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tbPenalty = New System.Windows.Forms.TextBox
        Me.tbName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblTimeOrSheet = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'tbPenalty
        '
        Me.tbPenalty.Location = New System.Drawing.Point(150, 59)
        Me.tbPenalty.Name = "tbPenalty"
        Me.tbPenalty.Size = New System.Drawing.Size(41, 20)
        Me.tbPenalty.TabIndex = 2
        '
        'tbName
        '
        Me.tbName.Location = New System.Drawing.Point(150, 24)
        Me.tbName.Name = "tbName"
        Me.tbName.Size = New System.Drawing.Size(83, 20)
        Me.tbName.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Penalty:"
        '
        'lblTimeOrSheet
        '
        Me.lblTimeOrSheet.AutoSize = True
        Me.lblTimeOrSheet.Location = New System.Drawing.Point(12, 24)
        Me.lblTimeOrSheet.Name = "lblTimeOrSheet"
        Me.lblTimeOrSheet.Size = New System.Drawing.Size(57, 13)
        Me.lblTimeOrSheet.TabIndex = 3
        Me.lblTimeOrSheet.Text = "<on load>:"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(160, 110)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(83, 42)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(46, 110)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 42)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Add"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'AddSheetOrTime
        '
        Me.AcceptButton = Me.Button1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(289, 262)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbPenalty)
        Me.Controls.Add(Me.tbName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblTimeOrSheet)
        Me.Name = "AddSheetOrTime"
        Me.Text = "AddSheetOrTime"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbPenalty As System.Windows.Forms.TextBox
    Friend WithEvents tbName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTimeOrSheet As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
