Module Solver

    Public Sub ScheduleTeamPairs2()
        'Objective: fill TeamPairs(,) array with the number of games each pair plays
        Dim RandomTeamNum() As Integer
        Dim DivisionTeams() As Integer
        Dim NRoundsThisDiv As Integer
        Dim NTeamsThisRound As Integer
        Dim iteam As Integer = 0

        Dim kteam As Integer
        Dim kkteam As Integer
        Dim kround As Integer

        Dim kgame As Integer

        ReDim NTeamGames(NTeams)
        ReDim TeamPair(NTeams, NTeams)

        ReDim OrderedGameTeam1(NDivs, MaxDivGamesNeeded)  'by division, max number of games per div
        ReDim OrderedGameTeam2(NDivs, MaxDivGamesNeeded)

        Dim RRResults(,) As Integer
        For jdiv As Integer = 1 To NDivs
            RRResults = RoundRobin.GenerateRoundRobin(TeamsPerDiv(jdiv))
            ReDim DivisionTeams(TeamsPerDiv(jdiv) - 1), RandomTeamNum(TeamsPerDiv(jdiv) - 1)
            iteam = -1
            For jteam As Integer = 1 To NTeams 'TeamsPerDiv(jdiv)
                If TeamDiv(jteam) = jdiv Then
                    iteam += 1
                    DivisionTeams(iteam) = jteam
                End If
            Next
            RoundRobin.RandomOutput1(DivisionTeams, RandomTeamNum)

            NTeamsThisRound = TeamsPerDiv(jdiv) - 1

            NRoundsThisDiv = NTeamsThisRound - 1 'NGamesPerTeamDiv(jdiv) - 1
            If DivisionTeams.Length Mod 2 > 0 Then NRoundsThisDiv += 1 'And NGamesPerTeamDiv(jdiv) Mod 2 > 0 Then NRoundsThisDiv += 1
            kgame = 0
            For jround As Integer = 0 To NRoundsThisDiv
                kround = jround
                For jteam As Integer = 0 To NTeamsThisRound
                    kteam = RandomTeamNum(jteam)

                    If kround > DivisionTeams.Length - 1 Then kround = kround - DivisionTeams.Length
                    If RRResults(jteam, kround) >= 0 Then
                        kkteam = RandomTeamNum(RRResults(jteam, kround))
                        If (NTeamGames(kteam) < NGamesPerTeamDiv(jdiv) And NTeamGames(kkteam) < NGamesPerTeamDiv(jdiv)) And TeamPair(kteam, kkteam) = 0 Then
                            TeamPair(kteam, kkteam) += 1
                            TeamPair(kkteam, kteam) += 1
                            NTeamGames(kteam) += 1
                            NTeamGames(kkteam) += 1
                            kgame += 1
                            OrderedGameTeam1(jdiv, kgame) = kteam
                            OrderedGameTeam2(jdiv, kgame) = kkteam
                        End If
                    End If
                Next
            Next
            'check full schedule
            For jteam As Integer = 0 To NTeamsThisRound
                kteam = RandomTeamNum(jteam)
                If NTeamGames(kteam) < NGamesPerTeamDiv(jdiv) Then
                    'search for an unscheduled matchup
                    For jround As Integer = 0 To NRoundsThisDiv
                        kround = jround
                        If kround > DivisionTeams.Length - 1 Then kround = kround - DivisionTeams.Length
                        If RRResults(jteam, kround) >= 0 Then
                            kkteam = RandomTeamNum(RRResults(jteam, kround))
                            If TeamPair(kteam, kkteam) = 0 Then
                                TeamPair(kteam, kkteam) += 1
                                TeamPair(kkteam, kteam) += 1
                                NTeamGames(kteam) += 1
                                NTeamGames(kkteam) += 1
                                kgame += 1
                                OrderedGameTeam1(jdiv, kgame) = kteam
                                OrderedGameTeam2(jdiv, kgame) = kkteam
                                Exit For
                            End If
                        End If
                    Next
                End If
            Next
        Next jdiv

    End Sub
    Public Sub ScheduleTimesOrdered(ByRef binfeasible As Boolean)
        'allows for flexibility to schedule multiple pairings of the same two teams, as would be the case for a small pool of teams playing a large number of games
        Dim scheduled As Boolean
        Dim kteam As Integer
        Dim kkteam As Integer
        Dim TeamPairToSchedule(,) As Integer
        Dim TotalToSchedule As Integer = 0
        Dim TotalScheduled As Integer = 0
        Dim rweek As Integer
        Dim kweek As Integer

        Dim rtime As Integer
        Dim ktime As Integer

        Dim rsheet As Integer
        Dim ksheet As Integer

        Dim rdiv As Integer

        Dim WeekUsed() As Integer
        Dim nbyesused As Integer

        Dim DivGameIndex() As Integer
        Dim DivChecked() As Integer
        Dim nDivsChecked As Integer
        Dim DivGameScheduled(,) As Integer
        Dim DivCheckedThisWeek() As Integer
        Dim kind As Integer

        Dim tskip As Single
        Dim bfeaspair As Boolean

        binfeasible = False

        ReDim WeekTimeSheetUsed(NWeeks, NTimes, NSheets), MatchWeekTimeSheetTeam1(NWeeks, NTimes, NSheets), MatchWeekTimeSheetTeam2(NWeeks, NTimes, NSheets)
        ReDim NTeamWeek(NTeams, NWeeks)
        ReDim NTeamSheet(NTeams, NSheets)
        ReDim NTeamTime(NTeams, NTimes)
        ReDim DivCheckedThisWeek(NWeeks)
        ReDim DivGameIndex(NDivs)
        ReDim WeekUsed(NWeeks)
        ReDim DivGameScheduled(NDivs, MaxDivGamesNeeded)

        For jteam As Integer = 1 To NTeams
            For jjteam As Integer = 1 To NTeams
                TotalToSchedule += TeamPair(jteam, jjteam)
            Next
        Next

 
        ReDim TeamPairToSchedule(NTeams, NTeams)
        For jteam As Integer = 1 To NTeams
            For jjteam As Integer = 1 To NTeams
                TeamPairToSchedule(jteam, jjteam) = TeamPair(jteam, jjteam)
            Next
        Next

        nbyesused = 0
        Do Until TotalScheduled = TotalToSchedule

            'find a place and time to play
            Do
                rweek = CInt(Int(NWeeks * Rnd()) + 1) ' CInt(NWeeks * Rnd()) + 1
                rweek = Math.Min(rweek, NWeeks)
                If WeekUsed(rweek) = 0 Then Exit Do
            Loop
            kweek = rweek
            WeekUsed(kweek) = 1

            rtime = CInt(Int(NTimes * Rnd()))
            rsheet = CInt(Int(NSheets * Rnd()))

            'schedule games this week
            For jtime As Integer = 1 To NTimes
                ktime = jtime + rtime
                If ktime > NTimes Then
                    ktime = ktime - NTimes
                End If
                For jsheet As Integer = 1 To NSheets
                    scheduled = False
                    ksheet = jsheet + rsheet
                    If ksheet > NSheets Then
                        ksheet = ksheet - NSheets
                    End If
                    If nbyesused < NOpenSheets Then
                        tskip = Rnd() * (NOpenSheets + NGamesNeeded)
                        If tskip < NOpenSheets Then
                            nbyesused += 1
                            GoTo skipme
                        End If
                    End If
                    ReDim DivChecked(NDivs)
                    nDivsChecked = 0
                    Do
                        rdiv = CInt(Int(NDivs * Rnd()) + 1)
                        If rdiv > NDivs Then rdiv = NDivs
                        If DivChecked(rdiv) = 0 Then
                            DivChecked(rdiv) = 1
                            nDivsChecked += 1
                        End If
                        bfeaspair = False
                        'are there any possible games to schedule in this division this week?
                        For jind As Integer = 1 To GamesNeededPerDiv(rdiv)
                            If NTeamWeek(OrderedGameTeam1(rdiv, jind), kweek) = 0 And NTeamWeek(OrderedGameTeam2(rdiv, jind), kweek) = 0 Then
                                'yes we have a feasible pair
                                bfeaspair = True
                                Exit For
                            End If
                        Next
                        If DivGameIndex(rdiv) < GamesNeededPerDiv(rdiv) And bfeaspair = True Then Exit Do
                        If nDivsChecked = NDivs Then Exit Do
                    Loop
                    DivGameIndex(rdiv) += 1
                    kind = 1 'DivGameIndex(rdiv)
                    Do Until kind > GamesNeededPerDiv(rdiv)
                        kteam = OrderedGameTeam1(rdiv, kind)
                        kkteam = OrderedGameTeam2(rdiv, kind)

                        If WeekTimeSheetUsed(kweek, ktime, ksheet) = 0 And NTeamWeek(kteam, kweek) = 0 And NTeamWeek(kkteam, kweek) = 0 And DivGameScheduled(rdiv, kind) = 0 Then

                            WeekTimeSheetUsed(kweek, ktime, ksheet) = 1
                            MatchWeekTimeSheetTeam1(kweek, ktime, ksheet) = Math.Min(kteam, kkteam)
                            MatchWeekTimeSheetTeam2(kweek, ktime, ksheet) = Math.Max(kteam, kkteam)
                            SetTeamWeekTimeSheet(kteam)
                            SetTeamWeekTimeSheet(kkteam)
                            TeamPairToSchedule(kteam, kkteam) -= 1
                            TeamPairToSchedule(kkteam, kteam) -= 1
                            TotalScheduled += 2
                            scheduled = True
                            DivGameScheduled(rdiv, kind) = 1
                            Exit Do
                        Else
                            kind += 1
                            'If kind > DivGameIndex(rdiv) Then Stop
                        End If
                    Loop
                    If TotalScheduled >= TotalToSchedule Then Exit Do
                    If scheduled = False Then
                        binfeasible = True
                        Exit Sub
                    End If
skipme:
                Next
            Next
        Loop
        scheduled = scheduled
    End Sub
    Public Sub ScheduleTimes(ByRef binfeasible As Boolean)
        'allows for flexibility to schedule multiple pairings of the same two teams, as would be the case for a small pool of teams playing a large number of games
        Dim scheduled As Boolean
        Dim kteam As Integer
        Dim kkteam As Integer
        Dim TeamPairToSchedule(,) As Integer
        Dim TotalToSchedule As Integer = 0
        Dim TotalScheduled As Integer = 0
        Dim rweek As Integer
        Dim kweek As Integer

        Dim rtime As Integer
        Dim ktime As Integer

        Dim rsheet As Integer
        Dim ksheet As Integer

        binfeasible = False

        ReDim WeekTimeSheetUsed(NWeeks, NTimes, NSheets), MatchWeekTimeSheetTeam1(NWeeks, NTimes, NSheets), MatchWeekTimeSheetTeam2(NWeeks, NTimes, NSheets)
        ReDim NTeamWeek(NTeams, NWeeks)
        ReDim NTeamSheet(NTeams, NSheets)
        ReDim NTeamTime(NTeams, NTimes)

        For jteam As Integer = 1 To NTeams
            For jjteam As Integer = 1 To NTeams
                TotalToSchedule += TeamPair(jteam, jjteam)
            Next
        Next


        ReDim TeamPairToSchedule(NTeams, NTeams)
        For jteam As Integer = 1 To NTeams
            For jjteam As Integer = 1 To NTeams
                TeamPairToSchedule(jteam, jjteam) = TeamPair(jteam, jjteam)
            Next
        Next


        Do Until TotalScheduled = TotalToSchedule
            kteam = CInt(Int((NTeams * Rnd()) + 1)) 'CInt(NTeams * Rnd()) + 1
            kkteam = CInt(Int((NTeams * Rnd()) + 1)) 'CInt(NTeams * Rnd()) + 1
            If kteam > NTeams Then kteam = NTeams
            If kkteam > NTeams Then kkteam = NTeams

            'For jteam As Integer = 0 To NTeams - 1
            '    kteam = kteam + jteam
            '    If kteam > NTeams Then kteam = kteam - NTeams
            '    For jjteam As Integer = 0 To NTeams - 1
            '        kkteam = kkteam + jjteam
            '        If kkteam > NTeams Then kkteam = kkteam - NTeams
            '        If TeamPairToSchedule(kteam, kkteam) > 0 Then Exit For
            '    Next
            'Next
            Do Until TeamPairToSchedule(kteam, kkteam) > 0
                kteam = CInt(Int((NTeams * Rnd()) + 1)) 'CInt(NTeams * Rnd()) + 1
                kkteam = CInt(Int((NTeams * Rnd()) + 1)) 'CInt(NTeams * Rnd()) + 1
                If kteam > NTeams Then kteam = NTeams
                If kkteam > NTeams Then kkteam = NTeams
            Loop


            'If TeamPairToSchedule(kteam, kkteam) > 0 Then
            'find a place and time to play
            rweek = CInt(Int(NWeeks * Rnd())) ' CInt(NWeeks * Rnd()) + 1
            rtime = CInt(Int(NTimes * Rnd()))
            rsheet = CInt(Int(NSheets * Rnd()))
            scheduled = False

            For jweek As Integer = 1 To NWeeks
                kweek = jweek + rweek
                If kweek > NWeeks Then
                    kweek = kweek - NWeeks
                End If

                For jtime As Integer = 1 To NTimes
                    ktime = jtime + rtime
                    If ktime > NTimes Then
                        ktime = ktime - NTimes
                    End If
                    For jsheet As Integer = 1 To NSheets
                        ksheet = jsheet + rsheet
                        If ksheet > NSheets Then
                            ksheet = ksheet - NSheets
                        End If
                        If WeekTimeSheetUsed(kweek, ktime, ksheet) = 0 And NTeamWeek(kteam, kweek) = 0 And NTeamWeek(kkteam, kweek) = 0 Then

                            WeekTimeSheetUsed(kweek, ktime, ksheet) = 1
                            MatchWeekTimeSheetTeam1(kweek, ktime, ksheet) = Math.Min(kteam, kkteam)
                            MatchWeekTimeSheetTeam2(kweek, ktime, ksheet) = Math.Max(kteam, kkteam)
                            SetTeamWeekTimeSheet(kteam)
                            SetTeamWeekTimeSheet(kkteam)
                            TeamPairToSchedule(kteam, kkteam) -= 1
                            TeamPairToSchedule(kkteam, kteam) -= 1
                            TotalScheduled += 2
                            scheduled = True
                            Exit For
                        End If
                    Next
                    If scheduled = True Then Exit For
                Next
                If scheduled = True Then Exit For
            Next
            If scheduled = False Then
                binfeasible = True
                Exit Sub
            End If
            'End If
        Loop

    End Sub

    Public Sub RefineTimes()
        'moves a team with bad schedules to open slots
        Dim kteam As Integer
        Dim kkteam As Integer
        Dim kteampen As Integer
        Dim kkteampen As Integer
        Dim iteam As Integer
        'Dim maxpenalty As Integer = 0
        Dim nswitch As Integer

        Dim timepen As Integer
        Dim sheetpen As Integer
        Dim timeoffpen As Integer
        Dim teamgamepen As Integer
        Dim nteamswitched() As Integer

        Dim kpoints As Integer
        Dim maxpoints As Integer
        Dim points As Integer
        Dim maxQueue() As Integer
        Dim TeamPoints() As Integer


        ReDim nteamswitched(NTeams)



        nswitch = 1
        'For jiter As Integer = 1 To NRefineIters
        Do Until nswitch = 0

            If nswitch = 0 Then Exit Do 'For
            nswitch = 0

            ReDim TeamPoints(NTeams), maxQueue(NTeams)
            'find the team to evaluate
            maxpoints = 0
            For jteam As Integer = 1 To NTeams
                'kdiv = TeamDiv(jteam)
                CountTeamPoints(jteam, TeamSchedule, points)
                TeamPoints(jteam) = points
                If points > maxpoints Then 'And kteamTested(jteam) = 0 Then
                    maxpoints = points
                    kteam = jteam
                End If
            Next
            If kteam = 0 Then Exit Do
            Dim kind As Integer = 0

            'find the max queue
            kpoints = maxpoints + 1
            kind = 0
            For jqueue As Integer = 0 To maxpoints - 1
                kpoints -= 1
                For jteam As Integer = 1 To NTeams
                    If TeamPoints(jteam) = kpoints Then
                        kind += 1
                        maxQueue(kind) = jteam
                    End If
                Next
            Next


            'maxpenalty = 0
            For jteam As Integer = 1 To NTeams
                iteam = maxQueue(jteam)
                kteampen = TeamTimePenalty(iteam) + TeamSheetPenalty(iteam) + TeamTimeOffPenalty(iteam)


                If kteampen > 0 And nteamswitched(iteam) < NTeams Then

                    'find a match that causes the break (source)
                    For jweek As Integer = 1 To NWeeks
                        If NTeamWeek(iteam, jweek) = 1 Then
                            For jtime As Integer = 1 To NTimes
                                For jsheet As Integer = 1 To NSheets
                                    If MatchWeekTimeSheetTeam1(jweek, jtime, jsheet) = iteam Or MatchWeekTimeSheetTeam2(jweek, jtime, jsheet) = iteam Then
                                        kteam = iteam
                                        If MatchWeekTimeSheetTeam1(jweek, jtime, jsheet) <> kteam Then
                                            kkteam = MatchWeekTimeSheetTeam1(jweek, jtime, jsheet)
                                        Else
                                            kkteam = MatchWeekTimeSheetTeam2(jweek, jtime, jsheet)
                                        End If
                                        kkteampen = TeamTimePenalty(kkteam) + TeamSheetPenalty(kkteam) + TeamTimeOffPenalty(kkteam)

                                        'is this causing part of the problem?
                                        If (TeamTimePenalty(kteam) > 0 And TimePenalty(jtime) > 0) Or (TeamSheetPenalty(kteam) > 0 And SheetPenalty(jsheet) > 0) Then

                                            'find a potential spot to swap - week must be open for both teams
                                            For jjweek As Integer = 1 To NWeeks
                                                If (NTeamWeek(kteam, jjweek) = 0 And NTeamWeek(kkteam, jjweek) = 0) Or jweek = jjweek Then
                                                    For jjtime As Integer = 1 To NTimes
                                                        For jjsheet As Integer = 1 To NSheets
                                                            If WeekTimeSheetUsed(jjweek, jjtime, jjsheet) = 0 Then
                                                                'nswitch += 1
                                                                If (TeamTimePenalty(kteam) > 0 And TimePenalty(jjtime) = 0) Or (TeamSheetPenalty(kteam) > 0 And SheetPenalty(jjsheet) = 0) Then
                                                                    'do the switch
                                                                    MatchWeekTimeSheetTeam1(jweek, jtime, jsheet) = 0
                                                                    MatchWeekTimeSheetTeam2(jweek, jtime, jsheet) = 0
                                                                    MatchWeekTimeSheetTeam1(jjweek, jjtime, jjsheet) = Math.Min(kteam, kkteam)
                                                                    MatchWeekTimeSheetTeam2(jjweek, jjtime, jjsheet) = Math.Max(kteam, kkteam)

                                                                    SetWeekTimeSheetUsed()

                                                                    SetTeamWeekTimeSheet(kteam)
                                                                    SetTeamWeekTimeSheet(kkteam)
                                                                    MakeTeamSchedule(kteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                                                                    MakeTeamSchedule(kkteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)

                                                                    CountPenalties(timepen, sheetpen, timeoffpen, teamgamepen)
                                                                    nswitch += 1
                                                                    nteamswitched(kteam) += 1
                                                                    nteamswitched(kkteam) += 1
                                                                    GoTo Break
                                                                End If

                                                            End If
                                                        Next

                                                    Next
                                                End If

                                            Next
                                        End If
                                    End If

                                Next
                            Next
                        End If
                    Next


                End If
Break:
            Next
        Loop
        'Next jiter


    End Sub

    Public Sub FillOpenTimes()
        'looks through each week and fills undesirable open times
        '############Could be refined to identify the team that needs the switch the most, rather than just the first team-pair encountered
        Dim MaxPenWeek As Integer
        Dim MinPenWeekOpen As Integer
        Dim sourcektime As Integer
        Dim sourceksheet As Integer
        Dim sinkktime As Integer
        Dim sinkksheet As Integer
        Dim kteam As Integer
        Dim kkteam As Integer
        Dim kslot As Integer
        Dim openswitch As Integer
        Dim p1 As Integer
        Dim p2 As Integer
        Dim p3 As Integer
        Dim p4 As Integer

        For jweek As Integer = 1 To NWeeks

            openswitch = 1
            Do Until openswitch = 0
                openswitch = 0
                kslot = 0
                MinPenWeekOpen = 100000
                MaxPenWeek = 0
                sourcektime = 0
                sourceksheet = 0
                sinkktime = 0
                sinkksheet = 0

                For jtime As Integer = 1 To NTimes
                    For jsheet As Integer = 1 To NSheets
                        kslot += 1
                        'is this sheet open?
                        If WeekTimeSheetUsed(jweek, jtime, jsheet) = 0 Then
                            If SlotPenalty(kslot) < MinPenWeekOpen Then
                                MinPenWeekOpen = SlotPenalty(kslot)
                                sinkktime = jtime
                                sinkksheet = jsheet
                            End If
                        ElseIf WeekTimeSheetUsed(jweek, jtime, jsheet) > 0 Then
                            If SlotPenalty(kslot) > MaxPenWeek Then
                                MaxPenWeek = SlotPenalty(kslot)
                                sourcektime = jtime
                                sourceksheet = jsheet
                            End If
                        End If
                    Next
                Next

                If MinPenWeekOpen < MaxPenWeek Then 'do the switch
                    kteam = MatchWeekTimeSheetTeam1(jweek, sourcektime, sourceksheet)
                    kkteam = MatchWeekTimeSheetTeam2(jweek, sourcektime, sourceksheet)

                    'now do the switch if you found an improvement
                    MatchWeekTimeSheetTeam1(jweek, sourcektime, sourceksheet) = 0
                    MatchWeekTimeSheetTeam2(jweek, sourcektime, sourceksheet) = 0
                    MatchWeekTimeSheetTeam1(jweek, sinkktime, sinkksheet) = Math.Min(kteam, kkteam)
                    MatchWeekTimeSheetTeam2(jweek, sinkktime, sinkksheet) = Math.Max(kteam, kkteam)
    
                    SetWeekTimeSheetUsed()

                    SetTeamWeekTimeSheet(kteam)
                    SetTeamWeekTimeSheet(kkteam)
                    MakeTeamSchedule(kteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                    MakeTeamSchedule(kkteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                    CountPenalties(p1, p2, p3, p4) 'reseuts the penalty counters per team for next feasibility check

                    openswitch += 1
                End If
            Loop
        Next



    End Sub

    Public Sub TeamSwaps()
        'looks for swapping matches between team-level matches to look for reductions in points for a team

        Dim points As Integer
        Dim maxpoints As Integer
        Dim kteam As Integer
        Dim kteamTested() As Integer
        Dim minQueue() As Integer
        Dim maxQueue() As Integer
        Dim kpoints As Integer
        Dim NQueue As Integer
        Dim TeamPoints() As Integer
        Dim xteam As Integer
        Dim xxteam As Integer
        Dim yteam As Integer
        Dim yyteam As Integer
        Dim tempsched() As Integer
        Dim fromslot As Integer
        Dim toslot As Integer
        Dim bgamethisweek As Integer
        Dim ntests As Integer
        Dim p1 As Integer
        Dim p2 As Integer
        Dim p3 As Integer
        Dim p4 As Integer

        Dim Nswaps As Integer

        ReDim kteamTested(NTeams), minQueue(NTeams), TeamPoints(NTeams), maxQueue(NTeams)
        Nswaps = 1
        ntests = 0
        Do Until Nswaps = 0 Or ntests >= NTeams
            Nswaps = 0
            ReDim minQueue(NTeams), TeamPoints(NTeams), maxQueue(NTeams)
            'find the team to evaluate
            maxpoints = 0
            For jteam As Integer = 1 To NTeams
                'kdiv = TeamDiv(jteam)
                CountTeamPoints(jteam, TeamSchedule, points)
                TeamPoints(jteam) = points
                If points > maxpoints Then 'And kteamTested(jteam) = 0 Then
                    maxpoints = points
                    kteam = jteam
                End If
            Next
            If kteam = 0 Then Exit Do
            Dim kind As Integer = 0
            'find the test queue
            For jqueue As Integer = 0 To maxpoints - 1
                For jteam As Integer = 1 To NTeams
                    If TeamPoints(jteam) = jqueue Then
                        kind += 1
                        NQueue = kind
                        minQueue(kind) = jteam
                    End If
                Next
            Next
            'find the max queue
            kpoints = maxpoints + 1
            kind = 0
            For jqueue As Integer = 0 To maxpoints - 1
                kpoints -= 1
                For jteam As Integer = 1 To NTeams
                    If TeamPoints(jteam) = kpoints Then
                        kind += 1
                        maxQueue(kind) = jteam
                    End If
                Next
            Next

            'kteamTested(kteam) = 1
            ntests += 1
            'now go through all the team's games. Look for improvements without breaking the schedule of the teams you are swapping with
            For jmaxteam As Integer = 1 To NTeams
                kteam = maxQueue(jmaxteam)
                'now go through all the team's games. Look for improvements without breaking the schedule of the teams you are swapping with
                For jqueue As Integer = 1 To NQueue
                    If kteam <> minQueue(jqueue) And TeamPoints(kteam) >= TeamPoints(minQueue(jqueue)) And TeamDiv(kteam) = TeamDiv(minQueue(jqueue)) Then
                        For jweek As Integer = 1 To NWeeks
                            bgamethisweek = False
                            'check to make sure you have a game this week
                            For jjslot As Integer = 1 To NSlots
                                If TeamSchedule(kteam, jweek) = jjslot Then
                                    bgamethisweek = True
                                    Exit For
                                End If
                            Next
                            If bgamethisweek = True Then

                                For jjweek As Integer = 1 To NWeeks
                                    For kslot As Integer = 1 To NSlots
                                        'are we considering a swap with the min?
                                        If TeamSchedule(minQueue(jqueue), jjweek) = kslot Then
                                            If SlotPenalty(kslot) < SlotPenalty(TeamSchedule(kteam, jweek)) Then
                                                fromslot = TeamSchedule(kteam, jweek)
                                                toslot = kslot
                                                'consider the swap - need to evaluate the feasibility of all teams involved
                                                xteam = kteam
                                                yteam = minQueue(jqueue)
                                                If xteam <> MatchWeekTimeSheetTeam1(jweek, SlotTime(TeamSchedule(kteam, jweek)), SlotSheet(TeamSchedule(kteam, jweek))) Then
                                                    xxteam = MatchWeekTimeSheetTeam1(jweek, SlotTime(TeamSchedule(kteam, jweek)), SlotSheet(TeamSchedule(kteam, jweek)))
                                                Else
                                                    xxteam = MatchWeekTimeSheetTeam2(jweek, SlotTime(TeamSchedule(kteam, jweek)), SlotSheet(TeamSchedule(kteam, jweek)))
                                                End If
                                                If yteam <> MatchWeekTimeSheetTeam1(jjweek, SlotTime(kslot), SlotSheet(kslot)) Then
                                                    yyteam = MatchWeekTimeSheetTeam1(jjweek, SlotTime(kslot), SlotSheet(kslot))
                                                Else
                                                    yyteam = MatchWeekTimeSheetTeam2(jjweek, SlotTime(kslot), SlotSheet(kslot))
                                                End If

                                                'try swapping the pairing - x for y
                                                If TeamPair(xteam, yyteam) = 0 And TeamPair(yteam, xxteam) = 0 Then

                                                    'have to make sure swapped teams are not already schedule for the week you are swapping to
                                                    If (jweek = jjweek) Or (NTeamWeek(xteam, jjweek) = 0 And NTeamWeek(yteam, jweek) = 0) Then 'And NTeamWeek(xxteam, jjweek) = 0 And NTeamWeek(yteam, jweek) = 0 And NTeamWeek(yyteam, jweek) = 0) Then
                                                        'test the swap for feasibility

                                                        'xteam
                                                        ReDim tempsched(NWeeks)
                                                        tempsched(jweek) = 0
                                                        tempsched(jjweek) = toslot
                                                        For tweek As Integer = 1 To NWeeks
                                                            If tweek <> jjweek And tweek <> jweek Then tempsched(tweek) = TeamSchedule(xteam, tweek)
                                                        Next
                                                        If bScheduleFeasibility(tempsched, xteam) = False Then Exit For

                                                        'yteam
                                                        ReDim tempsched(NWeeks)
                                                        tempsched(jjweek) = 0
                                                        tempsched(jweek) = fromslot
                                                        For tweek As Integer = 1 To NWeeks
                                                            If tweek <> jjweek And tweek <> jweek Then tempsched(tweek) = TeamSchedule(yteam, tweek)
                                                        Next
                                                        If bScheduleFeasibility(tempsched, yteam) = False Then Exit For


                                                        'if you've gotten to here, do the swap!
                                                        TeamPair(xteam, xxteam) = 0
                                                        TeamPair(xxteam, xteam) = 0
                                                        TeamPair(yteam, yyteam) = 0
                                                        TeamPair(yyteam, yteam) = 0
                                                        TeamPair(yteam, xxteam) = 1
                                                        TeamPair(xxteam, yteam) = 1
                                                        TeamPair(xteam, yyteam) = 1
                                                        TeamPair(yyteam, xteam) = 1

                                                        MatchWeekTimeSheetTeam1(jweek, SlotTime(fromslot), SlotSheet(fromslot)) = Math.Min(yteam, xxteam)
                                                        MatchWeekTimeSheetTeam2(jweek, SlotTime(fromslot), SlotSheet(fromslot)) = Math.Max(yteam, xxteam)

                                                        MatchWeekTimeSheetTeam1(jjweek, SlotTime(toslot), SlotSheet(toslot)) = Math.Min(xteam, yyteam)
                                                        MatchWeekTimeSheetTeam2(jjweek, SlotTime(toslot), SlotSheet(toslot)) = Math.Max(xteam, yyteam)
                                                        SetWeekTimeSheetUsed()

                                                        'If yteam > 0 Then
                                                        SetTeamWeekTimeSheet(yteam)
                                                        SetTeamWeekTimeSheet(yyteam)
                                                        MakeTeamSchedule(yteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                                                        MakeTeamSchedule(yyteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                                                        'End If

                                                        SetTeamWeekTimeSheet(xteam)
                                                        SetTeamWeekTimeSheet(xxteam)
                                                        MakeTeamSchedule(xteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                                                        MakeTeamSchedule(xxteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                                                        CountPenalties(p1, p2, p3, p4) 'reseuts the penalty counters per team for next feasibility check
                                                        Nswaps += 1
                                                        GoTo NextMax
                                                    End If
                                                End If
                                            End If
                                        End If
                                    Next

                                Next
                            End If
                        Next jweek
                    End If
                Next jqueue
            Next jmaxteam
NextMax:
        Loop

    End Sub

    Public Sub TeamPairSwaps()
        'looks for swapping matches between teampairs to look for reductions in points for a team

        Dim points As Integer
        Dim maxpoints As Integer
        Dim kteam As Integer
        Dim kteamTested() As Integer
        Dim MinQueue() As Integer
        Dim MaxQueue() As Integer
        Dim NQueue As Integer
        Dim TeamPoints() As Integer
        Dim xteam As Integer
        Dim xxteam As Integer
        Dim yteam As Integer
        Dim yyteam As Integer
        Dim tempsched() As Integer
        Dim fromslot As Integer
        Dim toslot As Integer
        Dim kpoints As Integer
        Dim ntests As Integer
        Dim p1 As Integer
        Dim p2 As Integer
        Dim p3 As Integer
        Dim p4 As Integer

        Dim Nswaps As Integer

        ReDim kteamTested(NTeams), MinQueue(NTeams), TeamPoints(NTeams), MaxQueue(NTeams)
        Nswaps = 1
        ntests = 0
        Do Until Nswaps = 0 Or ntests >= NTeams
            Nswaps = 0
            kteam = 0
            ReDim MinQueue(NTeams), TeamPoints(NTeams), MaxQueue(NTeams)
            'find the team to evaluate
            maxpoints = 0
            For jteam As Integer = 1 To NTeams
                'kdiv = TeamDiv(jteam)
                CountTeamPoints(jteam, TeamSchedule, points)
                TeamPoints(jteam) = points
                If points > maxpoints Then 'And kteamTested(jteam) = 0 Then
                    maxpoints = points
                    kteam = jteam
                End If
            Next
            If kteam = 0 Then Exit Do
            Dim kind As Integer = 0
            'find the test queue
            For jqueue As Integer = 0 To maxpoints - 1
                For jteam As Integer = 1 To NTeams
                    If TeamPoints(jteam) = jqueue Then
                        kind += 1
                        NQueue = kind
                        MinQueue(kind) = jteam
                    End If
                Next
            Next
            'find the max queue
            kpoints = maxpoints + 1
            kind = 0
            For jqueue As Integer = 0 To maxpoints - 1
                kpoints -= 1
                For jteam As Integer = 1 To NTeams
                    If TeamPoints(jteam) = kpoints Then
                        kind += 1
                        MaxQueue(kind) = jteam
                    End If
                Next
            Next

            ntests += 1
            For jmaxteam As Integer = 1 To NTeams
                kteam = MaxQueue(jmaxteam)
                'now go through all the team's games. Look for improvements without breaking the schedule of the teams you are swapping with
                For jqueue As Integer = 1 To NQueue
                    If kteam <> MinQueue(jqueue) And TeamPoints(kteam) >= TeamPoints(MinQueue(jqueue)) Then
                        For jweek As Integer = 1 To NWeeks
                            For jjweek As Integer = 1 To NWeeks
                                For kslot As Integer = 1 To NSlots
                                    'are we considering a swap with the min?
                                    If TeamSchedule(MinQueue(jqueue), jjweek) = kslot Then
                                        If SlotPenalty(kslot) < SlotPenalty(TeamSchedule(kteam, jweek)) Then
                                            fromslot = TeamSchedule(kteam, jweek)

                                            toslot = kslot
                                            'consider the swap - need to evaluate the feasibility of all teams involved
                                            xteam = MatchWeekTimeSheetTeam1(jweek, SlotTime(TeamSchedule(kteam, jweek)), SlotSheet(TeamSchedule(kteam, jweek)))
                                            xxteam = MatchWeekTimeSheetTeam2(jweek, SlotTime(TeamSchedule(kteam, jweek)), SlotSheet(TeamSchedule(kteam, jweek)))
                                            yteam = MatchWeekTimeSheetTeam1(jjweek, SlotTime(kslot), SlotSheet(kslot))
                                            yyteam = MatchWeekTimeSheetTeam2(jjweek, SlotTime(kslot), SlotSheet(kslot))
                                            'have to make sure no team is scheduled for the week of the swaps
                                            If (jweek = jjweek) Or (NTeamWeek(xteam, jjweek) = 0 And NTeamWeek(xxteam, jjweek) = 0 And NTeamWeek(yteam, jweek) = 0 And NTeamWeek(yyteam, jweek) = 0) Then
                                                'test the swap for feasibility

                                                'xteam
                                                ReDim tempsched(NWeeks)
                                                tempsched(jweek) = 0
                                                tempsched(jjweek) = toslot
                                                For tweek As Integer = 1 To NWeeks
                                                    If tweek <> jjweek And tweek <> jweek Then tempsched(tweek) = TeamSchedule(xteam, tweek)
                                                Next
                                                If bScheduleFeasibility(tempsched, xteam) = False Then Exit For

                                                'xxteam
                                                ReDim tempsched(NWeeks)
                                                tempsched(jweek) = 0
                                                tempsched(jjweek) = toslot
                                                For tweek As Integer = 1 To NWeeks
                                                    If tweek <> jjweek And tweek <> jweek Then tempsched(tweek) = TeamSchedule(xxteam, tweek)
                                                Next
                                                If bScheduleFeasibility(tempsched, xxteam) = False Then Exit For

                                                'yteam
                                                ReDim tempsched(NWeeks)
                                                tempsched(jjweek) = 0
                                                tempsched(jweek) = fromslot
                                                For tweek As Integer = 1 To NWeeks
                                                    If tweek <> jweek And tweek <> jjweek Then tempsched(tweek) = TeamSchedule(yteam, tweek)
                                                Next
                                                If bScheduleFeasibility(tempsched, yteam) = False Then Exit For

                                                'yyteam
                                                ReDim tempsched(NWeeks)
                                                tempsched(jjweek) = 0
                                                tempsched(jweek) = fromslot
                                                For tweek As Integer = 1 To NWeeks
                                                    If tweek <> jweek And tweek <> jjweek Then tempsched(tweek) = TeamSchedule(yyteam, tweek)
                                                Next
                                                If bScheduleFeasibility(tempsched, yyteam) = False Then Exit For

                                                'if you've gotten to here, do the swap!

                                                If yteam > 0 Then
                                                    MatchWeekTimeSheetTeam1(jweek, SlotTime(fromslot), SlotSheet(fromslot)) = yteam
                                                    MatchWeekTimeSheetTeam2(jweek, SlotTime(fromslot), SlotSheet(fromslot)) = yyteam
                                                    'WeekTimeSheetUsed(jweek, SlotTime(fromslot), SlotSheet(fromslot)) = 1
                                                End If
                                                MatchWeekTimeSheetTeam1(jjweek, SlotTime(toslot), SlotSheet(toslot)) = xteam 'Math.Min(kteam, kkteam)
                                                MatchWeekTimeSheetTeam2(jjweek, SlotTime(toslot), SlotSheet(toslot)) = xxteam 'Math.Max(kteam, kkteam)
                                                'WeekTimeSheetUsed(jjweek, SlotTime(toslot), SlotSheet(toslot)) = 1
                                                SetWeekTimeSheetUsed()

                                                If yteam > 0 Then
                                                    SetTeamWeekTimeSheet(yteam)
                                                    SetTeamWeekTimeSheet(yyteam)
                                                    MakeTeamSchedule(yteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                                                    MakeTeamSchedule(yyteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                                                End If

                                                SetTeamWeekTimeSheet(xteam)
                                                SetTeamWeekTimeSheet(xxteam)
                                                MakeTeamSchedule(xteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                                                MakeTeamSchedule(xxteam, MatchWeekTimeSheetTeam1, MatchWeekTimeSheetTeam2, NTeamWeek)
                                                CountPenalties(p1, p2, p3, p4) 'reseuts the penalty counters per team for next feasibility check
                                                Nswaps += 1
                                                GoTo NextMax
                                            End If
                                        End If
                                    End If
                                Next

                            Next
                        Next
                    End If
                Next jqueue
NextMax:
            Next jmaxteam
        Loop

    End Sub

    Public Sub CountPenalties(ByRef timepen As Integer, ByRef sheetpen As Integer, ByRef timeoffpen As Integer, ByRef teamgamepen As Integer)
        timepen = 0
        sheetpen = 0
        teamgamepen = 0
        timeoffpen = 0
        Dim kdiv As Integer
        Dim ktimepenalty As Integer
        Dim ksheetpenalty As Integer
        Dim ktimeoffpenalty As Integer
        Dim timeoffcounter As Integer
        ReDim TeamTimePenalty(NTeams), TeamSheetPenalty(NTeams), TeamTimeOffPenalty(NTeams)
        For jteam As Integer = 1 To NTeams
            ktimepenalty = 0
            ksheetpenalty = 0
            ktimeoffpenalty = 0
            kdiv = TeamDiv(jteam)

            For jtime As Integer = 1 To NTimes
                ktimepenalty += NTeamTime(jteam, jtime) * TimePenalty(jtime)
            Next
            If ktimepenalty > MaxPerTime Then
                TeamTimePenalty(jteam) = ktimepenalty - MaxPerTime
                timepen += TeamTimePenalty(jteam)
            End If

            For jsheet As Integer = 1 To NSheets
                ksheetpenalty += NTeamSheet(jteam, jsheet) * SheetPenalty(jsheet)
            Next
            If ksheetpenalty > MaxPerSheet Then
                TeamSheetPenalty(jteam) = ksheetpenalty - MaxPerSheet
                sheetpen += TeamSheetPenalty(jteam)
            End If

            timeoffcounter = 0
            For jweek As Integer = 1 To NWeeks
                If NTeamWeek(jteam, jweek) = 0 Then
                    timeoffcounter += 1
                    If timeoffcounter > MaxTimeOff Then
                        timeoffpen += 1
                        ktimeoffpenalty += 1
                    End If

                Else
                    timeoffcounter = 0
                End If
            Next
            TeamTimeOffPenalty(jteam) = ktimeoffpenalty

            If NTeamGames(jteam) < NGamesPerTeamDiv(kdiv) Then
                teamgamepen += NGamesPerTeamDiv(kdiv) - NTeamGames(jteam)
            End If

        Next
    End Sub

    Public Sub CountTeamPenalties(ByRef teamgamepenalty As Integer)
        teamgamepenalty = 0
        Dim kdiv As Integer
        For jteam As Integer = 1 To NTeams
            kdiv = TeamDiv(jteam)
            If NTeamGames(jteam) < NGamesPerTeamDiv(kdiv) Then
                teamgamepenalty += NGamesPerTeamDiv(kdiv) - NTeamGames(jteam)
            End If

        Next
    End Sub

    Public Sub CountTeamPoints(ByVal kteam As Integer, ByVal sched(,) As Integer, ByRef teampoints As Integer)
        teampoints = 0
        For jweek As Integer = 1 To NWeeks
            teampoints += SlotPenalty(sched(kteam, jweek))
        Next
    End Sub

    Public Sub MakeTeamSchedule(ByVal kteam As Integer, ByVal Team1MWTS(,,) As Integer, ByVal Team2MWTS(,,) As Integer, ByVal NTW(,) As Integer)
        Dim kslot As Integer
        'have to zero-out the team's kslots
        For jweek As Integer = 1 To NWeeks
            TeamSchedule(kteam, jweek) = 0
        Next
        'now, fill in the team schedule with the latest
        kslot = 0
        For jweek As Integer = 1 To NWeeks
            If NTW(kteam, jweek) = 1 Then
                kslot = 0
                For jtime As Integer = 1 To NTimes
                    For jsheet As Integer = 1 To NSheets
                        kslot += 1
                        If Team1MWTS(jweek, jtime, jsheet) = kteam Or Team2MWTS(jweek, jtime, jsheet) = kteam Then
                            TeamSchedule(kteam, jweek) = kslot
                        End If
                    Next jsheet
                Next jtime
            End If
        Next jweek

    End Sub

    Private Function bScheduleFeasibility(ByVal TestSched() As Integer, ByVal kteam As Integer) As Boolean
        bScheduleFeasibility = True
        'search for infeasibilities

        Dim ktime As Integer
        Dim ksheet As Integer
        Dim ktimepenalty As Integer
        Dim ksheetpenalty As Integer
        Dim ktimeoffpenalty As Integer
        Dim timeoffcounter As Integer

        ktimepenalty = 0
        ksheetpenalty = 0
        ktimeoffpenalty = 0


        For jweek As Integer = 1 To NWeeks

            ktime = SlotTime(TestSched(jweek))
            ksheet = SlotSheet(TestSched(jweek))
            ktimepenalty += TimePenalty(ktime)
            ksheetpenalty += SheetPenalty(ksheet)
            'If ktimepenalty > TeamTimePenalty(kteam) Or ksheetpenalty > TeamSheetPenalty(kteam) Then
            If ktimepenalty > Math.Max(MaxPerTime, TeamTimePenalty(kteam)) Or ksheetpenalty > Math.Max(MaxPerSheet, TeamSheetPenalty(kteam)) Then
                bScheduleFeasibility = False
                Exit Function
            End If
            If ktime = 0 Then
                timeoffcounter += 1
                If timeoffcounter > Math.Max(MaxTimeOff, TeamTimeOffPenalty(kteam)) Then
                    ktimeoffpenalty += 1
                    bScheduleFeasibility = False
                    Exit Function
                End If
            Else
                timeoffcounter = 0
            End If

        Next

    End Function

    Private Sub SetTeamWeekTimeSheet(ByVal kteam As Integer)

        'first reset stuff for this team
        For jweek As Integer = 1 To NWeeks
            NTeamWeek(kteam, jweek) = 0
        Next jweek
        For jtime As Integer = 1 To NTimes
            NTeamTime(kteam, jtime) = 0
        Next jtime
        For jsheet As Integer = 1 To NSheets
            NTeamSheet(kteam, jsheet) = 0
        Next jsheet


        'then factor the new schedule
        For jweek As Integer = 1 To NWeeks
            For jtime As Integer = 1 To NTimes
                For jsheet As Integer = 1 To NSheets
                    If MatchWeekTimeSheetTeam1(jweek, jtime, jsheet) = kteam Or MatchWeekTimeSheetTeam2(jweek, jtime, jsheet) = kteam Then
                        NTeamWeek(kteam, jweek) += 1
                        NTeamSheet(kteam, jsheet) += 1
                        NTeamTime(kteam, jtime) += 1
                        'WeekTimeSheetUsed(jweek, jtime, jsheet) = 1
                    End If
                Next
            Next
        Next

    End Sub

    Private Sub SetWeekTimeSheetUsed()
        ReDim WeekTimeSheetUsed(NWeeks, NTimes, NSheets)
        For jweek As Integer = 1 To NWeeks
            For jtime As Integer = 1 To NTimes
                For jsheet As Integer = 1 To NSheets
                    If MatchWeekTimeSheetTeam1(jweek, jtime, jsheet) > 0 Then
                        WeekTimeSheetUsed(jweek, jtime, jsheet) = 1
                    End If
                Next
            Next
        Next
    End Sub

    Public Sub AssignTeamSchedule(ByRef unscheduledbyes As Integer)
        Dim TeamOpen() As Integer
        ReDim TeamOpen(NTeams), TeamNumber(NTeams)
        ReDim TeamByeScheduled(NTeams, 2)
        Dim bscheduled As Boolean
        unscheduledbyes = 0

        'schedule bye1
        For jteam As Integer = 1 To NTeams
            bscheduled = False
            If TeamBye1(jteam) > 0 Then
                'look for an opening
                For jjteam As Integer = 1 To NTeams
                    For jweek As Integer = 1 To NWeeks
                        If TeamSchedule(jjteam, jweek) = 0 And TeamBye1(jteam) = jweek And TeamOpen(jjteam) = 0 And TeamDiv(jteam) = TeamDiv(jjteam) Then
                            TeamNumber(jjteam) = jteam
                            TeamOpen(jjteam) = 1
                            TeamByeScheduled(jjteam, 1) = TeamBye1(jteam)
                            bscheduled = True
                            Exit For
                        End If
                    Next
                    If bscheduled = True Then Exit For
                Next
                If bscheduled = False Then unscheduledbyes += 1
            End If
        Next
        'schedule bye2
        For jteam As Integer = 1 To NTeams
            bscheduled = False
            If TeamBye2(jteam) > 0 Then
                'look for an opening

                For jjteam As Integer = 1 To NTeams
                    For jweek As Integer = 1 To NWeeks
                        If TeamSchedule(jjteam, jweek) = 0 And TeamBye2(jteam) = jweek And (TeamOpen(jjteam) = 0 Or TeamNumber(jjteam) = jteam) And TeamDiv(jteam) = TeamDiv(jjteam) Then
                            'release the old assignment
                            'also have to preserve bye1
                            If TeamSchedule(jjteam, TeamBye1(jteam)) = 0 Then
                                For jjjteam As Integer = 1 To NTeams
                                    If TeamNumber(jjjteam) = jteam Then
                                        TeamNumber(jjjteam) = 0
                                        TeamOpen(jjjteam) = 0
                                        TeamByeScheduled(jjjteam, 1) = 0
                                    End If
                                Next
                                TeamByeScheduled(jjteam, 1) = TeamBye1(jteam)
                                TeamByeScheduled(jjteam, 2) = TeamBye2(jteam)
                                TeamNumber(jjteam) = jteam
                                TeamOpen(jjteam) = 1

                                bscheduled = True
                                Exit For
                            End If
                        End If
                    Next
                    If bscheduled = True Then Exit For
                Next
                If bscheduled = False Then unscheduledbyes += 1
            End If
        Next

        'schedule unscheduled
        For jteam As Integer = 1 To NTeams
            bscheduled = False
            For jjteam As Integer = 1 To NTeams
                If TeamNumber(jjteam) = jteam Then
                    bscheduled = True
                    Exit For
                End If
            Next
            If bscheduled = False Then
                For jjteam As Integer = 1 To NTeams
                    If TeamOpen(jjteam) = 0 And TeamDiv(jteam) = TeamDiv(jjteam) And TeamNumber(jjteam) = 0 Then
                        TeamNumber(jjteam) = jteam
                        TeamOpen(jjteam) = 1
                        Exit For
                    End If
                Next
            End If
        Next


    End Sub

End Module
