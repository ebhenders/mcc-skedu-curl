Public Class AddSheetOrTime

    Private Sub AddSheetOrTime_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = SheetOrTimeLabel

        bCancel = True
        lblTimeOrSheet.Text = TimeSheet.TimeOrSheet & " Name:"
        tbPenalty.Text = "0"
        If TimeSheet.bUsed = True Then
            tbName.Text = TimeSheet.Name
            tbPenalty.Text = TimeSheet.Penalty
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        TimeSheet.Name = tbName.Text
        If TimeSheet.Name.Length > 8 Then TimeSheet.Name = TimeSheet.Name.Substring(0, 8)
        TimeSheet.Penalty = CType(Trim(tbPenalty.Text), Integer)

        bCancel = False
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bCancel = True
        Me.Close()
    End Sub
End Class